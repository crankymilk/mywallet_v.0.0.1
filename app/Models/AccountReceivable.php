<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountReceivable extends Model
{
    use HasFactory;
     protected $table = "account_receivable";
     public $timestamps = false;

    public function connection()
	{
	    return $this->belongsTo(Connection::class, 'connection_id', 'id');
	}
}
