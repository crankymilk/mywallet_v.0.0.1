<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JournalTransaction extends Model
{
    use HasFactory;
     protected $table = "journal_transaction";
     public $timestamps = false;

    public function category()
	{
	    return $this->belongsTo(Category::class, 'category_id', 'id');
	}

    public function categoryGrandParent()
	{
	    return $this->belongsTo(Category::class, 'category_grandparent_id', 'id');
	}

	public function item()
	{
	    return $this->belongsTo(Item::class, 'item_id', 'id');
	}

	public function journal()
	{
	    return $this->belongsTo(Journal::class, 'journal_id', 'id');
	}
}
