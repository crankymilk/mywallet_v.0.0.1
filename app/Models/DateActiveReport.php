<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DateActiveReport extends Model
{
    use HasFactory;
    protected $table = "date_active_report";
     public $timestamps = false;
}
