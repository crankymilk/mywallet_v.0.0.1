<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountReceivableBill extends Model
{
    use HasFactory;

    protected $table = "account_receivable_bill";
    public $timestamps = false;

    public function ar()
	{
	    return $this->belongsTo(AccountReceivable::class, 'account_receivable_id', 'id');
	}

	public function transaction()
	{
	    return $this->belongsTo(JournalTransaction::class, 'journal_transaction_id', 'id');
	}
}
