<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Wallet;
use Auth;

class WalletController extends Controller
{
    public function enabled($wallet_id)
    {
    	Wallet::where('id',$wallet_id)->update([
    		'is_active' => '1'
    	]);

    	return redirect('dashboard');
    }

    public function disabled($wallet_id)
    {
    	Wallet::where('id',$wallet_id)->update([
    		'is_active' => '0'
    	]);

    	return redirect('dashboard');
    }

    public function ajaxUpdate()
    {

        $wallet_id = $_GET['wallet_id'];
        Wallet::whereIn('id',$wallet_id)->where('users_id',Auth::user()->id)->update([
            'is_active' => '1'
        ]);

        Wallet::whereNotIn('id',$wallet_id)->where('users_id',Auth::user()->id)->update([
            'is_active' => '0'
        ]);

        $wallets = Wallet::where('users_id', Auth::user()->id)->orderBy('name')->get();

        return response()->json([
            'html' => view('wallet.index',compact('wallets'))->render()
        ]);
    }

    public function ajaxStore()
    {
        $wallet = new Wallet;
        $wallet->name = $_GET['name'];
        $wallet->is_saving = $_GET['is_saving'];
        $wallet->is_active = '0';
        $wallet->users_id = Auth::user()->id;
        $wallet->save();

        $wallets = Wallet::where('users_id', $wallet->users_id)->orderBy('name')->get();

        return response()->json([
            'html' => view('wallet.index',compact('wallets'))->render()
        ]);
    }
}
