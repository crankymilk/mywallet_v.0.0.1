<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DateActiveReport;

class DateActiveController extends Controller
{
    public function update(Request $request)
    {
    	DateActiveController::where('users_id',Auth::user()->id)->update([
    		''
    	]);

    	$date = DateActiveReport::where('users_id',Auth::user()->id)->first();
    	$date->duration = $request->duration;

    	
    	
    	if($date->duration == 'one_month'){
    		if($request->custom_days == '1'){
	    		$date->start_days = $request->start_days;
		    	$date->end_days = 'custom';
		    	$date->custom_end_days = $request->end_days;
	    	}else{
	    		$date->end_days = 'auto';
	    	}
    	}else if($date->duration == 'last_month'){
    		$date->total_last_month = $request->last_month;
    	}else if($date->duration == 'last_year'){
    		$date->total_last_year = $request->last_year;
    	}
    	    	
    	$date->save();

    	return redirect('dashboard');
    }
}
