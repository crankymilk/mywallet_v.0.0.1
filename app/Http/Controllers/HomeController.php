<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JournalTransaction;
use App\Models\Wallet;
use App\Models\Journal;
use App\Models\AccountReceivableBill;
use App\Models\DateActiveReport;
use Auth;

class HomeController extends Controller
{
    public function index($start_date=false,$end_date=false)
    {
        //report

        if($start_date){
            $start_date = $start_date;
            $end_date = $end_date;
        }else{
            $date = $this->dateActive(date('m'),date('Y'));
            $start_date = $date[0];
            $end_date = $date[1];
        }

        $month = date('m',strtotime($start_date));
        $year = date('Y',strtotime($start_date));
        

        $beginning_balance = $this->beginningBalanceReport($start_date);
      

    	$category_income = $this->currentJournal('1',$month ,$year);
    	$total_income = 0;
    	foreach($category_income as $income){
            $income->journal_transaction = JournalTransaction::where('journal_id',$income->journal_id)->where('category_grandparent_id','1')->get()->sum('amount');
    		$total_income = $total_income + $income->journal_transaction;
    	}

        $category_ar = $this->currentJournal('3',$month ,$year);
        $total_ar = 0;
        foreach($category_ar as $ar){
            $giving = JournalTransaction::where('journal_id',$ar->journal_id)->where('category_id','32')->get()->sum('amount');
            $payment = JournalTransaction::where('journal_id',$ar->journal_id)->where('category_id','33')->get()->sum('amount');

            $ar->journal_transaction = $payment - $giving;

            $total_ar = $total_ar - $giving + $payment;
        }

    	$category_expanditure = $this->currentJournal('2',$month ,$year);
    	$total_expanditure = 0;
    	foreach($category_expanditure as $expanditure){
            $expanditure->journal_transaction = JournalTransaction::where('journal_id',$expanditure->journal_id)->where('category_grandparent_id','2')->get()->sum('amount');
    		$total_expanditure = $total_expanditure - $expanditure->journal_transaction;
    	}
        
        
        $category_saving = $this->currentJournal('36',$month,$year);
        $total_saving_a = 0;
        $total_saving_b = 0;
        foreach($category_saving as $saving){
            
             $saving->journal_transaction_a = JournalTransaction::where('journal_id',$saving->journal_id)->where('category_id','104')->where('category_grandparent_id','36')->get()->sum('amount');
             
             $saving->journal_transaction_b = JournalTransaction::where('journal_id',$saving->journal_id)->where('category_id','!=','104')->where('category_grandparent_id','36')->get()->sum('amount');
            
            $total_saving_a = $total_saving_a + $saving->journal_transaction_a;
            $total_saving_b = $total_saving_b + $saving->journal_transaction_b;
        }
        
        $total_saving = $total_saving_a + $total_saving_b;

    	$total_amount = $beginning_balance + $total_income + $total_ar + $total_expanditure + $total_saving_a - $total_saving_b;

        $wallets = Wallet::where('users_id',Auth::user()->id)->get();


        //notif
        $wallet = Wallet::select('id')->where('users_id',Auth::user()->id)->where('is_active','1')->first();

        $bills = Journal::where('need_confirmed', '1')->where('is_confirmed','0')->where('already_repeat','0')->where('wallet_id',$wallet->id)->where('repeat_next_date','<=',date('Y-m-d'))->get();

        $piutang = AccountReceivableBill::where('created_by',Auth::user()->id)->whereIn('status',['pending','postponed'])->where('duedate','<=',date('Y-m-d'))->get();
        foreach($piutang as $value){
            $value->balancing = AccountReceivableBill::where('created_by',Auth::user()->id)->whereIn('status',['pending','postponed'])->where('account_receivable_id',$value->account_receivable_id)->sum('amount');
            $value->paid = AccountReceivableBill::where('created_by',Auth::user()->id)->where('status','paid')->where('account_receivable_id',$value->account_receivable_id)->sum('paid');
            $value->merge = AccountReceivableBill::where('created_by',Auth::user()->id)->where('status','merge')->where('account_receivable_id',$value->account_receivable_id)->get()->count();
        }

        $balancing_wallet_active = Wallet::where('users_id',Auth::user()->id)->where('is_active','1')->get()->sum('balancing');

        
        return view('dashboard.index',compact('category_income','category_expanditure','category_ar','category_saving','total_income','total_ar','total_expanditure','total_saving','total_amount','wallets','beginning_balance','balancing_wallet_active','start_date','end_date','bills','piutang'));
    }

    public function dateActive($month,$year)
    {
        $date_active = DateActiveReport::where('users_id',Auth::user()->id)->first();
        if($date_active->duration == 'one_month'){
            //$current_date = date('Y-m-d');
            $current_days = $month;
            $current_month = $month;
            $last_month = $current_month-1;
            $next_month = $current_month+1;
            

            if($date_active->end_days == 'custom'){
                $start_date = $year."-".$last_month."-".$date_active->start_days;
                $end_date =  $year."-".$current_month."-".$date_active->custom_end_days;

                if($current_days >= 1 AND $current_days <= 22){
                    $start_date = $year."-".$last_month."-".$date_active->start_days;
                    $end_date = $year."-".$current_month."-".$date_active->custom_end_days;
                }else if($current_days >= 23 AND $current_days <= 31){
                    $start_date = $year."-".$current_month."-".$date_active->start_days;
                    $end_date =  $year."-".$next_month."-".$date_active->custom_end_days;
                }
            }else{
                $start_date = $year."-".$month."-01";
                $end_date = $year."-".$month."-".date('t',strtotime($start_date));
            }

        }
        else if($date_active->duration == 'one_year'){
            $start_date = $year."-01-01";
            $end_date = $year."-12-31";
        }
        else if($date_active->duration == 'last_month'){
            $end_date = date('Y-m-d');
            $last = '-'.$date_active->total_last_month.' month';
            $start_date = date('Y-m-d', strtotime($last, $end_date));
        }
        else if($date_active->duration == 'last_year'){
            $end_date = date('Y-m-d');
            $last = '-'.$date_active->total_last_year.' year';
            $start_date = date('Y-m-d', strtotime($last, $end_date));
        }

        $dateActive = [$start_date,$end_date];

        return $dateActive;
    }

    public function currentJournal($category_id,$month,$year,$is_beginning_balance=false)
    {
        $wallet = Wallet::select('id')->where('users_id',Auth::user()->id)->where('is_active','1')->get();

        $date = $this->dateActive($month,$year);

        $journal = JournalTransaction::select('journal.title as title','journal.total_amount as total_amount','journal.date as date','journal.id as journal_id')
                        ->join('journal','journal_id','journal.id')
                        ->where('created_by',Auth::user()->id)
                        ->whereIn('journal.wallet_id',$wallet);

        
       
                        
        $journal = $journal->where('date','>=',$date[0])
                        ->where('date','<=',$date[1])
                        ->where('category_grandparent_id',$category_id)
                        ->groupBy('journal_id')
                        ->orderBy('journal.date','ASC')
                        ->get();

        return $journal;
    }

    public function beginningBalanceReport($start_date)
    {
        $wallet = Wallet::select('id')->where('users_id',Auth::user()->id)->where('is_active','1')->get();

        $total_income = JournalTransaction::join('journal','journal.id','journal_id')->where('created_by',Auth::user()->id)
                        ->whereIn('journal.wallet_id',$wallet)->where('journal.date','<',$start_date)->where('category_grandparent_id','1')->get()->sum('amount');
        $total_expanditure = JournalTransaction::join('journal','journal.id','journal_id')->where('created_by',Auth::user()->id)
                        ->whereIn('journal.wallet_id',$wallet)->where('journal.date','<',$start_date)->where('category_grandparent_id','2')->get()->sum('amount');

        $total_ar_giving = JournalTransaction::join('journal','journal.id','journal_id')->where('created_by',Auth::user()->id)
                        ->whereIn('journal.wallet_id',$wallet)->where('journal.date','<',$start_date)->where('category_id','32')->get()->sum('amount');
        $total_ar_payment = JournalTransaction::join('journal','journal.id','journal_id')->where('created_by',Auth::user()->id)
                        ->whereIn('journal.wallet_id',$wallet)->where('journal.date','<',$start_date)->where('category_id','33')->get()->sum('amount');

        /*$saving->journal_transaction_a = JournalTransaction::where('journal_id',$saving->journal_id)->where('category_id','104')->where('category_grandparent_id','36')->get()->sum('amount');
        $saving->journal_transaction_b = JournalTransaction::where('journal_id',$saving->journal_id)->where('category_id','!=','104')->where('category_grandparent_id','36')->get()->sum('amount');*/
        $total_saving = JournalTransaction::join('journal','journal.id','journal_id')->where('journal.date','<',$start_date)->where('category_grandparent_id','36')->get()->sum('amount');
        

        $total_amount = $total_income - $total_ar_giving +  $total_ar_payment - $total_expanditure  + $total_saving;

        return $total_amount;
    }

    

}
