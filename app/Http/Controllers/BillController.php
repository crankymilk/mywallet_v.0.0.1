<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Wallet;
use App\Models\AccountReceivable;
use App\Models\AccountReceivableBill;
use App\Models\Connection;
use App\Models\Journal;
use App\Models\JournalTag;
use App\Models\JournalTransaction;
use App\Models\Unit;
use App\Models\Item;
use Ramsey\Uuid\Uuid;
use Auth;

class BillController extends Controller
{
    public function index()
    {
    	$wallet = Wallet::select('id')->where('users_id',Auth::user()->id)->where('is_active','1')->first();

        $bills = Journal::where('need_confirmed', '1')->where('is_confirmed','0')->where('already_repeat','0')->where('wallet_id',$wallet->id)->where('repeat_next_date','<=',date('Y-m-d'))->get();



        $bills_with_enddate = Journal::where('need_confirmed', '1')->where('is_confirmed','0')->where('already_repeat','0')->where('wallet_id',$wallet->id)->where('repeat_next_date','<=',date('Y-m-d'))->where('repeat_end_date','<=',date('Y-m-d'))->get();

        $mergebills = Journal::where('need_confirmed', '1')->where('is_confirmed','1')->where('already_repeat','0')->where('wallet_id',$wallet->id)->get();

        return view('bill.index',compact('bills','mergebills','bills_with_enddate'));
    }

    public function paid($journal_id)
    {
    	$journal = Journal::find($journal_id);

    	//new journal
    	$newJournal = new Journal;
    	$newJournal->date = date('Y-m-d');
    	$newJournal->title = $journal->title;
    	$newJournal->description = $journal->description;
    	$newJournal->wallet_id = $journal->wallet_id;
    	$newJournal->total_amount = $journal->total_amount;
    	$newJournal->is_repeat = $journal->is_repeat;
    	$newJournal->repeat_duration = $journal->repeat_duration;
    	$newJournal->repeat_duration_times = $journal->repeat_duration_times;
    	$newJournal->repeat_start_date = date('Y-m-d');

    	$date = $newJournal->repeat_start_date;
    	if($newJournal->repeat_duration == "daily"){
    			$newdate = strtotime ( '+'.$newJournal->repeat_duration_times.' day' , strtotime ( $date ) ) ;
    	}else if($newJournal->repeat_duration == "monthly"){
    			$newdate = strtotime ( '+'.$newJournal->repeat_duration_times.' month' , strtotime ( $date ) ) ;
    	}else if($newJournal->repeat_duration == "yearly"){
    			$newdate = strtotime ( '+'.$newJournal->repeat_duration_times.' year' , strtotime ( $date ) ) ;
    	}
			
    	$newJournal->repeat_next_date = date('Y-m-d',$newdate);
    	$newJournal->repeat_end_definition = $journal->repeat_end_definition;
    	$newJournal->repeat_end_date = $journal->repeat_end_date;
    	$newJournal->need_confirmed = $journal->need_confirmed;
    	$newJournal->status = 'paid';
    	$newJournal->created_by = $journal->created_by;
    	$newJournal->save();

    	$trans = JournalTransaction::where('journal_id',$journal->id)->get();

    	foreach ($trans as $key => $value) {
    		//new transaction
    		$newTrans = new JournalTransaction;
    		$newTrans->journal_id = $newJournal->id;
	    	$newTrans->category_id = $value->category_id;
			$newTrans->category_parent_id = $value->category_parent_id;
	        $newTrans->category_grandparent_id = $value->category_grandparent_id;
	    	$newTrans->item_id = $value->item_id;
	    	$newTrans->note = $value->note;
	    	$newTrans->total_item = $value->total_item;
	    	$newTrans->unit_price = $value->unit_price;
	    	$newTrans->amount = $value->amount;
	    	$newTrans->saving_to_wallet_id = $value->saving_to_wallet_id;

	    	$newTrans->save();

	    	$category = Category::where('id',$newTrans->category_id)->first();
    		$explode = explode("/", $category->root);

    		$newJournal = Journal::find($newJournal->id);
	    	if($explode[0] == '1'){
	            $newJournal->total_amount = $newJournal->total_amount + $newTrans->amount;
	        }else if($explode[0] == '2'){
	            $newJournal->total_amount = $newJournal->total_amount - $newTrans->amount;
	        }else if($explode[0] == '36'){
	            $newJournal->total_amount = $newJournal->total_amount - $newTrans->amount;
	        }else if($explode[0] == '3'){
	            if($explode[1] == '33'){
	                $newJournal->total_amount = $newJournal->total_amount + $newTrans->amount;
	            }else if($explode[1] == '32'){
	                $newJournal->total_amount = $newJournal->total_amount - $newTrans->amount;
	            }
	            
	        }else if($explode[0] == '4'){
	            if($explode[1] == '34'){
	                $newJournal->total_amount = $newJournal->total_amount + $newTrans->amount;
	            }else if($explode[1] == '35'){
	                $newJournal->total_amount = $newJournal->total_amount - $newTrans->amount;
	            }
	        }
	    	$newJournal->save();

	    	$wallet = Wallet::find($newJournal->wallet_id);
	        if($explode[0] == '1'){
	            $wallet->balancing = $wallet->balancing + $newTrans->amount;
	        }else if($explode[0] == '2'){
	            $wallet->balancing = $wallet->balancing - $newTrans->amount;
	        }else if($explode[0] == '36'){
	           $wallet->balancing = $wallet->balancing - $newTrans->amount;
	        }else if($explode[0] == '3'){
	            if($explode[1] == '33'){
	                $wallet->balancing = $wallet->balancing + $newTrans->amount;
	            }else if($explode[1] == '32'){
	                $wallet->balancing = $wallet->balancing - $newTrans->amount;
	            }
	            
	        }else if($explode[0] == '4'){
	            if($explode[1] == '34'){
	                $wallet->balancing = $wallet->balancing + $newTrans->amount;
	            }else if($explode[1] == '35'){
	                $wallet->balancing = $wallet->balancing - $newTrans->amount;
	            }
	        }
	        $wallet->save();
    	}

    	

    	$journal->is_confirmed = '1';
    	$journal->already_repeat = '1';
    	$journal->save();


        return redirect('bill');
    }

    public function merge($journal_id)
    {
    	$journal = Journal::find($journal_id);
    	$journal->is_confirmed = '1';
    	$journal->already_repeat = '0';
    	$journal->merge_times = $journal->merge_times + 1;
    	$journal->save();


        return redirect('bill');
    }
}
