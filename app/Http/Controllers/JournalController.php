<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Wallet;
use App\Models\AccountReceivable;
use App\Models\AccountReceivableBill;
use App\Models\Connection;
use App\Models\Journal;
use App\Models\JournalTag;
use App\Models\JournalTransaction;
use App\Models\Unit;
use App\Models\Item;
use Ramsey\Uuid\Uuid;
use Auth;

class JournalController extends Controller
{
    public function create()
    {
    	$category = Category::where('parent_id','0')->get();
    	$wallet = Wallet::where('users_id',Auth::user()->id)->get();

    	return view('report.create',compact('category','wallet'));
    }

    public function store(Request $request)
    {
    	$journal = new Journal;
    	$journal->date = $request->trans_date;
    	$journal->title =  $request->title;
    	$journal->description = $request->description;
    	$journal->wallet_id = $request->wallet;

    	$journal->total_amount = '0';

    	$journal->is_repeat = $request->is_repeat == '1' ? '1' : '0';

    	if($journal->is_repeat == '1'){
            $uuid4 = Uuid::uuid4();

            $journal->repeat_unique_id = $uuid4.date('YmdHis');
    		$journal->repeat_duration = $request->repeat_duration;
    		$journal->repeat_duration_times = $request->repeat_duration_times;


    		$journal->repeat_start_date = $journal->date;

    		$date = $journal->repeat_start_date;
    		if($journal->repeat_duration == "daily"){
    			$newdate = strtotime ( '+'.$journal->repeat_duration_times.' day' , strtotime ( $date ) ) ;
    		}else if($journal->repeat_duration == "monthly"){
    			$newdate = strtotime ( '+'.$journal->repeat_duration_times.' month' , strtotime ( $date ) ) ;
    		}else if($journal->repeat_duration == "yearly"){
    			$newdate = strtotime ( '+'.$journal->repeat_duration_times.' year' , strtotime ( $date ) ) ;
    		}
			
    		
    		$journal->repeat_next_date = date('Y-m-d',$newdate);
	    	$journal->repeat_end_definition = $request->repeat_end_definition == '1' ? '1' : '0';

	    	if($journal->repeat_end_definition == '1'){
	    		$journal->repeat_end_date = $request->repeat_end_date;
	    	}
	    	
	    	$journal->need_confirmed = $request->need_confirmed == '1' ? '1' : '0';
    	}

    	$journal->status = "paid";
		    	$journal->created_by = Auth::user()->id;

    	$journal->save();

        $journal_id = $journal->id;


    	$tags = explode(" ",$request->tags);

    	foreach($tags as $tag){
    		if (strpos($tag, '#') !== false) {
			    $journalTag = new JournalTag;
		    	$journalTag->journal_id = $journal_id;
		    	$journalTag->name = $tag;
		    	$journalTag->created_by = Auth::user()->id;
		    	$journalTag->save();
			}
    	}
    	
    	

    	return redirect('journal/'.$journal_id);
    }

    public function delete($journal_id){
        $journal = Journal::find($journal_id);
        $journalTrans = JournalTransaction::where('journal_id',$journal_id)->get();

        foreach($journalTrans as $value){
            $wallet = Wallet::find($journal->wallet_id);

            if($value->category_grandparent_id == '1'){ //pendapatan dihapus balancing berkurang
                $newBalancing = $wallet->balancing - $value->amount;
            }else if($value->category_grandparent_id == '2'){ //pengeluaran dihapus balancing bertambah
                $newBalancing = $wallet->balancing + $value->amount;
            }else if($value->category_grandparent_id == '3'){ //AR dihapus 
                if($value->category_id == "32"){ //jika giving maka balancing bertambah
                    $newBalancing = $wallet->balancing + $value->amount;
                }else{ //jika payment maka balancing berkurang
                    $newBalancing = $wallet->balancing - $value->amount;
                }
                
            }else if($value->category_grandparent_id == '4'){ //DEBT dihapus 
                if($value->category_id == "32"){ //jika request maka balancing berkurang
                    $newBalancing = $wallet->balancing - $value->amount;
                }else{ //jika payment maka balancing bertambah
                    $newBalancing = $wallet->balancing + $value->amount;
                }
                
            }else if($value->category_grandparent_id == '36'){ //tabungan dihapus balancing bertambah
                $newBalancing = $wallet->balancing + $value->amount;
            }

            Wallet::where('id',$journal->wallet_id)->update([
                'balancing' => $newBalancing
            ]);
        }

        JournalTransaction::where('journal_id',$journal_id)->delete();
        Journal::where('id',$journal_id)->delete();

        return redirect()->back();
    }

 	public function show($id)
 	{
 		$journal = Journal::find($id);
 		$category_income = JournalTransaction::where('category_grandparent_id','1')->where('journal_id',$id)->groupBy('category_id')->orderBy('id','ASC')->get();

        $total_income = 0;
        foreach($category_income as $income){
            $income->total_amount = 0;
            $income->transaction = JournalTransaction::where('journal_id',$id)->where('category_id',$income->category_id)->get();

            foreach ($income->transaction as $transaction) {
                $total_income = $total_income + $transaction->amount;
                $income->total_amount = $income->total_amount + $transaction->amount;
            }
        }

        $category_ar = JournalTransaction::where('category_grandparent_id','3')->where('journal_id',$id)->groupBy('category_id')->orderBy('id','ASC')->get();

        $total_ar = 0;
        foreach($category_ar as $ar){
            $ar->total_amount = 0;
            $ar->transaction = JournalTransaction::where('journal_id',$id)->where('category_id',$ar->category_id)->get();

            foreach ($ar->transaction as $transaction) {
                $total_ar = $total_ar + $transaction->amount;
                $ar->total_amount = $ar->total_amount + $transaction->amount;


                $transaction->payment_bill = AccountReceivableBill::where('journal_transaction_id',$transaction->id)->get();
            }
        }

 		$category_expanditure = JournalTransaction::where('category_grandparent_id','2')->where('journal_id',$id)->groupBy('category_id')->orderBy('id','ASC')->get();

 		$total_expanditure = 0;
 		foreach($category_expanditure as $expanditure){
 			$expanditure->total_amount = 0;
 			$expanditure->transaction = JournalTransaction::where('journal_id',$id)->where('category_id',$expanditure->category_id)->get();

 			foreach ($expanditure->transaction as $transaction) {
 				$total_expanditure = $total_expanditure + $transaction->amount;
 				$expanditure->total_amount = $expanditure->total_amount + $transaction->amount;

 			}
 		}

        $category_saving = JournalTransaction::where('category_grandparent_id','36')->where('journal_id',$id)->groupBy('category_id')->orderBy('id','ASC')->get();

        $total_saving = 0;
        foreach($category_saving as $saving){
            $saving->total_amount = 0;
            $saving->transaction = JournalTransaction::where('journal_id',$id)->where('category_id',$saving->category_id)->get();

            foreach ($saving->transaction as $transaction) {
                $total_saving = $total_saving + $transaction->amount;
                $saving->total_amount = $saving->total_amount + $transaction->amount;
            }
        }

 		return view('report.detail',compact('journal','category_income','total_income','category_ar','total_ar','category_expanditure','total_expanditure','category_saving','total_saving'));
 	}   

 	public function createTrans($id)
    {
    	$first_categories = Category::where('parent_id','0')->get();

    	foreach($first_categories as $first_category){
    		$first_category->child = Category::where('parent_id',$first_category->id)->whereNotIn('id',['33'])->get();

    		foreach($first_category->child as $second_category){
    			$second_category->child = Category::where('parent_id',$second_category->id)->get();
    		}
    	}

    	$units = Unit::orderBy('name')->get();
    	$journal_id = $id;

    	$items = Item::orderby('name')->get();

    	return view('report.createTrans',compact('first_categories','units','journal_id','items'));
    }

    public function arPaymentHistory($transaction_id)
    {
        $payment_bill = AccountReceivableBill::where('journal_transaction_id',$transaction_id)->get();

        return view('report.ar.payment_history',compact('payment_bill'));
    }

    public function storeTrans(Request $request)
    {

    	$category = Category::where('id',$request->cat_id)->first();

    	$explode = explode("/", $category->root);

    	$journalTrans = new JournalTransaction;
    	$journalTrans->journal_id = $request->journal_id;
    	$journalTrans->category_id = $request->cat_id;


    	if(count($explode) > 2){
            $journalTrans->category_parent_id = $category->parent_id;
    		$journalTrans->category_grandparent_id = $explode[0];
    	}else if(count($explode) == 2){
            $journalTrans->category_parent_id = '0';
            $journalTrans->category_grandparent_id = $explode[0];
        }
    	
    	$journalTrans->item_id = $request->item_id == "" ? "0" : $request->item_id;
    	$journalTrans->note = $request->note;
    	$journalTrans->total_item = $request->total_item;
    	$journalTrans->unit_price = $request->unit_price;
    	$journalTrans->amount = $request->total_price;
    	$journalTrans->saving_to_wallet_id = $request->wallet_id;

    	$journalTrans->save();

    	$journal = Journal::find($request->journal_id);

        if($explode[0] == '1'){
            $journal->total_amount = $journal->total_amount + $journalTrans->amount;
        }else if($explode[0] == '2'){
            $journal->total_amount = $journal->total_amount - $journalTrans->amount;
        }else if($explode[0] == '36'){
            $journal->total_amount = $journal->total_amount - $journalTrans->amount;
        }else if($explode[0] == '3'){
            if($explode[1] == '33'){
                $this->paidArBill($request,$request->connection_id);

                $journal->total_amount = $journal->total_amount + $journalTrans->amount;
            }else if($explode[1] == '32'){
                $connection_id = $this->inputConnection($request);
                $this->inputArBill($request,$connection_id,$journalTrans->id);

                $journal->total_amount = $journal->total_amount - $journalTrans->amount;
            }
            
        }else if($explode[0] == '4'){
            if($explode[1] == '34'){
                $journal->total_amount = $journal->total_amount + $journalTrans->amount;
            }else if($explode[1] == '35'){
                $journal->total_amount = $journal->total_amount - $journalTrans->amount;
            }
        }
    	

    	$journal->save();

        $wallet = Wallet::find($journal->wallet_id);

        if($explode[0] == '1'){
            $wallet->balancing = $wallet->balancing + $journalTrans->amount;
        }else if($explode[0] == '2'){
            $wallet->balancing = $wallet->balancing - $journalTrans->amount;
        }else if($explode[0] == '36'){
           $wallet->balancing = $wallet->balancing - $journalTrans->amount;
        }else if($explode[0] == '3'){
            if($explode[1] == '33'){
                $wallet->balancing = $wallet->balancing + $journalTrans->amount;
            }else if($explode[1] == '32'){
                $wallet->balancing = $wallet->balancing - $journalTrans->amount;
            }
            
        }else if($explode[0] == '4'){
            if($explode[1] == '34'){
                $wallet->balancing = $wallet->balancing + $journalTrans->amount;
            }else if($explode[1] == '35'){
                $wallet->balancing = $wallet->balancing - $journalTrans->amount;
            }
        }

        $wallet->save();

    	if($journalTrans->saving_to_wallet_id != ""){
    		$journalSaving = new Journal;
	    	$journalSaving->date = $journal->date;
	    	$journalSaving->title =  $journalTrans->note;
	    	$journalSaving->description = "This journal automatically input from a transaction in journal '".$journal->title."'";
	    	$journalSaving->wallet_id = $journalTrans->saving_to_wallet_id;
	    	$journalSaving->total_amount = $journalTrans->amount;
            $journalSaving->is_repeat = '0';
            $journalSaving->copied_journal_id = $journal->id;
            $journalSaving->copied_journal_trans_id = $journalTrans->id;
            $journalSaving->status = 'paid';
	    	$journalSaving->created_by = Auth::user()->id;
	    	$journalSaving->save();

	    	$journalTransSaving = new JournalTransaction;
	    	$journalTransSaving->journal_id = $journalSaving->id;
	    	$journalTransSaving->category_id = '103';
    		$journalTransSaving->category_grandparent_id = '1';

	    	$journalTransSaving->note = 'Transfer from wallet: '.$wallet->name;
    		$journalTransSaving->amount = $journalTrans->amount;
    		$journalTransSaving->save();

            $walletSaving = Wallet::find($journalSaving->wallet_id);
            $walletSaving->balancing = $walletSaving->balancing + $journal->total_amount;
            $walletSaving->save();

    	}

    	return redirect('journal/'.$request->journal_id);
    }

    public function inputConnection($request)
    {
        $connection = new Connection;
        $connection->name = $request->receiver_name;
        $connection->phone = $request->receiver_phone;
        $connection->email = $request->receiver_email;
        $connection->save();

        return $connection->id;
    }

    public function inputArBill($request,$connection_id,$journal_transaction_id)
    {
        $ar = new AccountReceivable;
        $ar->journal_transaction_id = $journal_transaction_id;
        $ar->connection_id = $connection_id;
        $ar->total_bill = $request->credit;
        $ar->total_ar = $request->total_price;
        $ar->total_paid = 0;
        $ar->save();

        $amount_per_bill = $ar->total_ar / $ar->total_bill;

        
        for($i=1;$i<=$ar->total_bill;$i++){
            $arBill = new AccountReceivableBill;

            if($i > 1){
                if($request->repeat_duration == "daily"){
                    $date = strtotime ( '+'.$i.' day' , strtotime ( $request->start_date ) ) ;
                }else if($request->repeat_duration == "weekly"){
                    $date = strtotime ( '+'.$i.' week' , strtotime ( $request->start_date ) ) ;
                }else if($request->repeat_duration == "monthly"){
                    $date = strtotime ( '+'.$i.' month' , strtotime ( $request->start_date ) ) ;
                }else if($request->repeat_duration == "yearly"){
                    $date = strtotime ( '+'.$i.' year' , strtotime ( $request->start_date ) ) ;
                }
            }else{
                $date = $request->start_date;
            }

            $arBill->number = $i;
            $arBill->duedate = $date;
            $arBill->account_receivable_id = $ar->id;
            $arBill->journal_transaction_id = $ar->journal_transaction_id;
            $arBill->amount = $amount_per_bill;
            $arBill->status = "pending";
            $arBill->created_by = Auth::user()->id;
            $arBill->paid = 0;
            $arBill->save();
        }

        return $ar->id;
    }

    public function confirmArBill($status,$bill_id,$amount)
    {
        $arBill = AccountReceivableBill::find($bill_id);

        if($status == "merge"){
            $arBill->paid = 0;
            $arBill->merge_date = date('Y-m-d');
            $arBill->merge_amount = $arBill->amount + $arBill->merge_amount;

            $nextNumberBill = $arBill->number + 1;
            $nextArBill = AccountReceivableBill::where('account_receivable_id',$arBill->account_receivable_id)
                        ->where('number',$nextNumberBill)->first();
            $nextArBill->merge_amount = $nextArBill->amount + $arBill->amount + $arBill->merge_amount;
            $nextArBill->save();
        }else if($status == "postponed"){
            $arBill->paid = 0;
        }else if($status == "paid"){
            $arBill->paid = $amount;
            $arBill->paid_date = date('Y-m-d');
        }
        
        $arBill->status = $status;
        $arBill->save();

        if($status == 'paid'){
            $wallet = Wallet::select('id')->where('users_id',Auth::user()->id)->where('is_active','1')->first();
            //new journal
            $newJournal = new Journal;
            $newJournal->date = date('Y-m-d');
            $newJournal->title = "Pembayaran hutang ".$arBill->ar->connection->name;
            $newJournal->description = "Pembayaran hutang bill ke-".$arBill->number;
            $newJournal->wallet_id = $wallet->id;
            $newJournal->total_amount = $arBill->paid;
            $newJournal->is_repeat = '0';                
            $newJournal->status = 'paid';
            $newJournal->created_by = Auth::user()->id;
            $newJournal->save();


            $journalTrans = new JournalTransaction;
            $journalTrans->journal_id = $newJournal->id;
            $journalTrans->category_id = '33';
            $journalTrans->category_parent_id = '0';
            $journalTrans->category_grandparent_id = '3';            
            $journalTrans->item_id = 0;
            $journalTrans->note = $newJournal->description;
            $journalTrans->amount = $arBill->paid;
            $journalTrans->save();


            $wallet = Wallet::find($newJournal->wallet_id);
            $wallet->balancing = $wallet->balancing + $journalTrans->amount;
            $wallet->save();

            $ar = AccountReceivable::find($arBill->account_receivable_id);
            $ar->total_paid = $ar->total_paid + $arBill->paid;
            $ar->save();

            $arBill = AccountReceivableBill::find($bill_id);
            $arBill->journal_payment_id = $newJournal->id;
            $arBill->save();
        }

        return redirect()->back();
    }

    public function deleteTrans($journal_id,$transaction_id)
    {
    	$copied_journal = Journal::where('copied_journal_trans_id',$transaction_id)->first();
        if($copied_journal){
            JournalTransaction::where('journal_id',$copied_journal->id)->delete();
        }
        
        $journalTrans = JournalTransaction::find($transaction_id);
        $journal = Journal::find($journal_id);

        $category = Category::where('id',$journalTrans->category_id)->first();

        $explode = explode("/", $category->root);

        if($explode[0] == '1'){
            $journal->total_amount = $journal->total_amount - $journalTrans->amount;
        }else if($explode[0] == '2'){
            $journal->total_amount = $journal->total_amount + $journalTrans->amount;
        }else if($explode[0] == '36'){
            $journal->total_amount = $journal->total_amount + $journalTrans->amount;
        }else if($explode[0] == '3'){
            if($explode[1] == '33'){
                $journal->total_amount = $journal->total_amount - $journalTrans->amount;
            }else if($explode[1] == '32'){
                $ar = AccountReceivable::where('journal_transaction_id',$journalTrans->id)->first();

                if($ar){
                    AccountReceivableBill::where('account_receivable_id',$ar->id)->delete();
                    AccountReceivable::where('journal_transaction_id',$journalTrans->id)->delete();
                }
                

                $journal->total_amount = $journal->total_amount + $journalTrans->amount;
            }
            
        }else if($explode[0] == '4'){
            if($explode[1] == '34'){
                $journal->total_amount = $journal->total_amount - $journalTrans->amount;
            }else if($explode[1] == '35'){
                $journal->total_amount = $journal->total_amount + $journalTrans->amount;
            }
        }
        

        $journal->save();

        $wallet = Wallet::find($journal->wallet_id);

        if($explode[0] == '1'){
            $wallet->balancing = $wallet->balancing - $journalTrans->amount;
        }else if($explode[0] == '2'){
            $wallet->balancing = $wallet->balancing + $journalTrans->amount;
        }else if($explode[0] == '36'){
           $wallet->balancing = $wallet->balancing + $journalTrans->amount;
        }else if($explode[0] == '3'){
            if($explode[1] == '33'){
                $wallet->balancing = $wallet->balancing - $journalTrans->amount;
            }else if($explode[1] == '32'){
                $wallet->balancing = $wallet->balancing + $journalTrans->amount;
            }
            
        }else if($explode[0] == '4'){
            if($explode[1] == '34'){
                $wallet->balancing = $wallet->balancing - $journalTrans->amount;
            }else if($explode[1] == '35'){
                $wallet->balancing = $wallet->balancing + $journalTrans->amount;
            }
        }

        $wallet->save();

        JournalTransaction::where('id',$transaction_id)->delete();

    	return redirect('journal/'.$journal_id);
    }

	
    public function ajaxSearchItem()
	{
		$keyword = $_GET['keyword'];

		$items = Item::where('name','LIKE',"%$keyword%")->orderby('name')->get();
		$units = Unit::orderBy('name')->get();
		
		return response()->json([
			'html' => view('item.list_option',compact('items','units'))->render()
		]);
	}    

	public function ajaxStoreItem()
	{
		$name = $_GET['name'];
		$unit = $_GET['unit'];
		$netto = $_GET['netto'];

		$item = new Item;
		$item->name = $name;
		$item->total = $netto;
		$item->unit_id = $unit;
		$item->save();

		$items = Item::orderby('name')->get();
		$units = Unit::orderBy('name')->get();

		return response()->json([
			'html' => view('item.list_option',compact('items','units'))->render()
		]);
	}   

	public function ajaxCreateTrans()
	{
		$cat_id = $_GET['cat_id'];

		$category = Category::find($cat_id);
		$explode = explode("/", $category->root);

		if($explode[0] == '2'){
			$units = Unit::orderBy('name')->get();
	    	$items = Item::orderby('name')->get();
	    	$need_item = $category->need_item;
	    	
			$view = view('report.createTransExpanditure',compact('items','units','need_item'))->render();
		}else if($explode[0] == '36'){
			$units = Unit::orderBy('name')->get();
	    	$items = Item::orderby('name')->get();
	    	$wallets = Wallet::where('users_id',Auth::user()->id)->where('is_saving','1')->get();
			$view = view('report.createTransSaving',compact('items','units','cat_id','wallets'))->render();
		}else if($explode[0] == '1'){
			$view = view('report.createTransIncome')->render();
		}else{
            if($explode[1] == '32'){
                $view = view('report.createTransAr')->render();
            }else{
                $view = "";
            }
			
		}

		return response()->json([
			'html' => $view
		]);
	}    
}
