<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\JournalController;
use App\Http\Controllers\WalletController;
use App\Http\Controllers\BillController;
use App\Http\Controllers\DateActiveController;
 
Route::get('/', [AuthController::class, 'showFormLogin'])->name('login');
Route::get('login', [AuthController::class, 'showFormLogin'])->name('login');
Route::post('login', [AuthController::class, 'login']);
Route::get('register', [AuthController::class, 'showFormRegister'])->name('register');
Route::post('register', [AuthController::class, 'register']);
 
Route::group(['middleware' => 'auth'], function () {
 
    Route::get('dashboard/{start_date?}/{end_date?}', [HomeController::class, 'index'])->name('dashboard');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

     Route::get('journal/create', [JournalController::class, 'create'])->name('create');
     Route::get('journal/{journal_id}', [JournalController::class, 'show']);
     Route::get('journal/delete/{journal_id}', [JournalController::class, 'delete']);
     Route::get('journal/{journal_id}/create-transaction', [JournalController::class, 'createTrans']);
     Route::post('journal/store', [JournalController::class, 'store'])->name('store');
     Route::post('journal/store-transaction', [JournalController::class, 'storeTrans']);
     Route::get('journal/{journal_id}/delete-transaction/{trans_id}', [JournalController::class, 'deleteTrans']);


     Route::get('wallet/active/{wallet_id}', [WalletController::class, 'enabled']);
     Route::get('wallet/inactive/{wallet_id}', [WalletController::class, 'disabled']);


     Route::get('bill', [BillController::class, 'index']);
     Route::get('bill/paid/{journal_id}', [BillController::class, 'paid']);
     Route::get('bill/merge/{journal_id}', [BillController::class, 'merge']);


     Route::get('ar/{status}/{bill_id}/{amount}', [JournalController::class, 'confirmArBill']);
     Route::get('ar/paid/payment/history/{transaction_id}', [JournalController::class, 'arPaymentHistory']);

     Route::get('date-active/update', [DateActiveController::class, 'update']);

     Route::get('ajax/item/search', [JournalController::class, 'ajaxSearchItem']);
     Route::get('ajax/item/store', [JournalController::class, 'ajaxStoreItem']);
     Route::get('ajax/journal/category/create', [JournalController::class, 'ajaxCreateTrans']);


     Route::get('ajax/wallet/update', [WalletController::class, 'ajaxUpdate']);
     Route::get('ajax/wallet/store', [WalletController::class, 'ajaxStore']);
 
});
