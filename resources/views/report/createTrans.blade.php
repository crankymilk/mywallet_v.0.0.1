@extends('app')
@section('content')

<div class="col-xl-12 col-md-12 mt-2">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Add Transaction</h4>
        <hr>

        <div class="row">
          <div class="col-md-12">
           	<form method="post" action="{{url('journal/store-transaction')}}">
                {{CSRF_FIELD()}}
                <input type="hidden" name="journal_id" value="{{$journal_id}}">
              <div class="form-group mt-3">
                  <label for="select">Category</label> 
                  <div>
                      <input type="hidden" id="cat_id" name="cat_id">
                      <input id="cat_name" name="cat_name" placeholder="Select Category" type="text" aria-describedby="nameHelpBlock" class="form-control" autocomplete="off" readonly onclick="openCategoryOption()"> 
                  </div>
              </div>

     

              <div id="form-create-by-cat"></div>


              <div class="form-group mt-4">
                <button name="submit" type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>

          </div>
        </div>

      </div>
    </div>

</div>
@endsection

@section('modalBottom1')
      <div class="row category-option" style="background: #e3e3e3a6;text-align: left;display: none;">
        <div class="col-12">
          <table class="table">
            <tr> 
              <td>
                <h3>Category Options</h3>

              </td>
              <td style="text-align: right;">
                <button class="btn btn-danger btn-sm" onclick="closeCategoryOption()">Cancel</button>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <span style="font-size: 15px;"><i>Click one of category below</i></span>
              </td>
            </tr>
          </table>
         

          <div style="overflow:auto;max-height: 300px;">
            <table class="table">
            
              @foreach($first_categories as $first_category)
              <tr onclick="openSecondCat('{{$first_category->id}}')">
                <td style="width: 10px;"><i class="{{$first_category->icon}}" style="font-size: 1.2rem;"></i></td>
                <td colspan="4">{{$first_category->name}} <br> <span style="font-size: 15px;"><i>({{$first_category->translate}})</i></span></td>
              </tr>
                @foreach($first_category->child as $second_category)

                @if(count($second_category->child) > 0)
                <tr class="child_{{$first_category->id}}" style="display: none;background: #f5f5f5;" onclick="openthirdCat('{{$first_category->id}}','{{$second_category->id}}')">
                  <td></td>
                  <td style="width: 10px;"><i class="{{$second_category->icon}}" style="font-size: 1.2rem;"></i></td>
                  <td colspan="2">{{$second_category->name}} <br> <span style="font-size: 15px;"><i>({{$second_category->translate}})</i></span></td>
                </tr>
                @else
                <tr class="child_{{$first_category->id}}" style="display: none;background: #f5f5f5;" onclick="chooseCat('{{$second_category->id}}','{{$first_category->name}} > {{$second_category->name}}')">
                  <td></td>
                  <td style="width: 10px;"><i class="{{$second_category->icon}}" style="font-size: 1.2rem;"></i></td>
                  <td colspan="2">{{$second_category->name}}<br> <span style="font-size: 15px;"><i>({{$second_category->translate}})</i></span></td>
                </tr>
                @endif

                  @foreach($second_category->child as $third_category)
                  <tr class="grandchild_{{$first_category->id}} child_{{$first_category->id}}_{{$second_category->id}}" style="display: none;background: white;" onclick="chooseCat('{{$third_category->id}}','{{$first_category->name}} > {{$second_category->name}} > {{$third_category->name}}')">
                    <td></td>
                    <td></td>
                    <td style="width: 10px;"><i class="{{$third_category->icon}}" style="font-size: 1.2rem;"></i></td>
                    <td>{{$third_category->name}}<br> <span style="font-size: 15px;"><i>({{$third_category->translate}})</i></span></td>
                  </tr>
                  @endforeach
                @endforeach
              @endforeach
            </table>
          </div>
          
        </div>
      </div>
@endsection

@section('modalBottom2')
      <!-- item option -->
      <div class="row item-option" style="background: #e3e3e3a6;text-align: left;display: none;">
        <div class="col-12">
          <table class="table">
            <tr> 
              <td>
                <h3>Item Options</h3>

              </td>
              <td style="text-align: right;">
                <button class="btn btn-danger btn-sm" onclick="closeItem()">Cancel</button>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <input type="text" class="form-control" name="keywordItem" id="keywordItem" placeholder="search item here" autofocus autocomplete="off" onkeyup="searchItem()">
              </td>
            </tr>
          </table>
         

          <div id="items-list" style="overflow:auto;max-height: 400px;">
            @include('item.list_option')
          </div>
          
        </div>
      </div>
@endsection


@section('script1')
<script type="text/javascript">

  function openSecondCat(id){
    $(".child_"+id).toggle();
    $(".grandchild_"+id).hide();
  }

  function openthirdCat(id1,id2){
    $(".child_"+id1+"_"+id2).toggle();
  }

  function closeCategoryOption(){
    $(".category-option").hide();
  }

  function openCategoryOption(){
    $(".category-option").show();
  }

  function chooseCat(cat_id,cat_name){
    $("#cat_id").val(cat_id);
    $("#cat_name").val(cat_name);

    var url = "{{url('ajax/journal/category/create')}}";

    $.ajax({
      type: "GET",
      url:url,
      data: {cat_id: cat_id},
      success: function(data) {
        $("#form-create-by-cat").html(data.html);
      },
      error: function(data) {
                successmessage = 'Error';
      },    
    });

    closeCategoryOption();
  }

  
  
</script>
@endsection

@section('scriptCreateExpanditure')
<script type="text/javascript">
  function calculate(keyup_from){
    var total_item = $("#total_item").val();
    var unit_price = $("#unit_price").val();
   // var shipping_cost = $("#shipping_cost").val();
  //  console.log(shipping_cost);
  //  if(shipping_cost == ""){
     //  $("#shipping_cost").val('0');
     //  shipping_cost = 0;
  //  }

    const array = [];

    if(total_item != ""){
      array.push(total_item);
    }

    if(unit_price != ""){
      array.push(unit_price);
    }

    console.log(array);
  
    var total_price = parseInt(total_item) * parseInt(unit_price);
        if(total_price < 0){
          total_price = 0;
    }

    
      $("#total_price").val(total_price);
    

  }

  function closeItem(){
    $(".item-option").hide();
  }

  function openItem(){
    $(".item-option").show();
  }

  function searchItem()
  {
    var keyword = $("#keywordItem").val();

    var url = "{{url('ajax/item/search')}}";
    var token = "{{csrf_token()}}";

    $.ajax({
      type: "GET",
      url:url,
      data: {keyword: keyword,_token:token},
      success: function(data) {
        $("#items-list").html(data.html);
      },
      error: function(data) {
                successmessage = 'Error';
      },    
    });

  }
</script>
@endsection