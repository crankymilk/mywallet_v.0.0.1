@extends('app')
@section('style')
<style type="text/css">
	
</style>
@endsection
@section('content')
<div class="col-xl-12 col-md-12 mt-2">
	@foreach($payment_bill as $bill)
		@php
			if($bill->status == "pending"){
				$bg ="#f3ae80";
			}else if($bill->status == "paid"){
				$bg ="#92c78e";
			}
		@endphp
    <div class="card" style="background:{{$bg}}">
      <div class="card-body">
        <h4 class="card-title">Bill ke-{{$bill->number}}</h4>
        <hr>
					<table class="table">
								         	
								         	<tr>
								         		<td><b>Duedate</b></td>
								         		<td><b>:</b></td>
								         		<td>{{date('j F Y',strtotime($bill->duedate))}}</td>
								         	</tr>
								         	<tr>
								         		<td><b>Bill Amount</b></td>
								         		<td><b>:</b></td>
								         		<td>Rp {{number_format($bill->amount,2, ',' , '.')}}</td>
								         	</tr>
								         	<tr>
								         		<td><b>Status</b></td>
								         		<td><b>:</b></td>
								         		<td>{{$bill->status}} on {{date('j F Y',strtotime(@$bill->paid_date))}}</td>
								         	</tr>
								         	<tr>
								         		<td><b>Paid Amount</b></td>
								         		<td><b>:</b></td>
								         		<td>Rp {{number_format($bill->paid,2, ',' , '.')}}</td>
								         	</tr>
								         </table>

      </div>
    </div>
  @endforeach
   
</div>
@endsection

@section('script1')
<script type="text/javascript">
	function openTransaction(id){
		$(".transaction_"+id).show();
		$(".caret_up_"+id).hide();
		$(".caret_down_"+id).show();
	}

	function closeTransaction(id){
		$(".transaction_"+id).hide();
		$(".caret_up_"+id).show();
		$(".caret_down_"+id).hide();
	}

</script>
@endsection