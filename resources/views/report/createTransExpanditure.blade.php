             @if($need_item == '1') <div class="form-group mt-3">
                  <label for="name">Item</label> 
                  <input type="hidden" id="item_id" name="item_id">
                  <input id="item_name" name="item_name" placeholder="Select Item" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control" autocomplete="off" onclick="openItem()"> 


              </div>

              <div class="form-group mt-3">
                  <label for="name">Total Item</label> 
                  <input id="total_item" name="total_item" placeholder="Type total item" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control" onkeyup="calculate('total_item')"> 
              </div>


              <div class="form-group mt-3">
                  <label for="name">Price Per Unit</label> 
                  <input id="unit_price" name="unit_price" placeholder="Type the price" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control" onkeyup="calculate('unit_price')"> 
              </div>

              {{--<div class="form-group mt-3">
                  <label for="name">Shipping Cost</label> 
                  <input id="shipping_cost" name="shipping_cost" placeholder="Type shipping cost" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control" value="0" onkeyup="calculate('shipping_cost')"> 
              </div>--}}
              

              <div class="form-group mt-3">
                  <label for="name">Total Price</label> 
                  <input id="total_price" name="total_price" placeholder="Type total price" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control" readonly onkeyup="calculate('total_price')"> 
              </div>
              
              @elseif($need_item == '0')
              
               <div class="form-group mt-3">
                  <label for="name">Note</label> 
                  <input id="note" name="note" placeholder="Type a note" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control"> 
              </div>

              <div class="form-group mt-3">
                  <label for="name">Price</label> 
                  <input id="total_price" name="total_price" placeholder="Type total price" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control"> 
              </div>
              @endif







