@extends('app')
@section('content')
<div class="col-xl-12 col-md-12 mt-2">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Input New Struk/Invoice</h4>
        <hr>

        <div class="row">
          <div class="col-md-12">
           	
           	<form method="post" action="{{url('journal/store')}}">
			  	{{CSRF_FIELD()}}
			  	<div class="form-group mt-3">
				    <label for="name">Transaction Date</label> 
				    <input id="trans_date" name="trans_date" type="date" required="required" class="form-control" value="{{date('Y-m-d')}}"> 
				    {{--<span id="nameHelpBlock" class="form-text text-muted">Max. 100 character</span>--}}
				</div>

				<div class="form-group mt-3">
				    <label for="select">Wallet</label> 
				    <div>
				        <select id="wallet" name="wallet" required="required" class="form-control">
					      	@foreach($wallet as $value)
					        <option value="{{$value->id}}">{{$value->name}}</option>
					        @endforeach
					    </select>
				    </div>
				</div>

				<div class="form-group mt-3">
				    <label for="name">Title</label> 
				    <input id="title" name="title" placeholder="Type a title" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control"> 
				    {{--<span id="nameHelpBlock" class="form-text text-muted">Max. 100 character</span>--}}
				</div>

			  <div class="form-group mt-3">
			    <label for="description">Description</label> 
			    <textarea id="description" name="description" cols="40" rows="5" required="required" class="form-control"></textarea>
			  </div>

			  

			  <div class="form-group mt-3">
			    <label for="select">Repeat this transaction</label> 
			    <div>
			        <div class="form-check form-switch">
					  <input class="form-check-input" type="checkbox" id="is_repeat" name="is_repeat" value="1" onclick="enabledRepeat()">
					  <label class="form-check-label" for="flexSwitchCheckDefault">Yes</label>
					</div>
			    </div>
			  </div>

			  <div class="form-group mt-3 repeat" style="display: none;">
			    <label for="select">Repeat this transaction every</label> 
			    <div>

			    	 <input id="repeat_duration_times" name="repeat_duration_times" placeholder="Repeat duration times" type="number" aria-describedby="nameHelpBlock"  class="form-control" style="margin-top: 1%;" value="1">

			        <select id="repeat_duration" name="repeat_duration"  class="form-control">
				        <option value="monthly">Month</option>
				        <option value="weekly">Week</option>
				        <option value="daily">Day</option>
				    </select>

			    </div>
			  </div>

			  <div class="form-group mt-3 repeat" style="display: none;">
				    <label for="name">Repeat end date</label> 
				    <div>
				        <div class="form-check form-switch">
						  <input class="form-check-input" type="checkbox" id="repeat_end_definition" name="repeat_end_definition" value="1" onclick="enabledEndDate()">
						  <label class="form-check-label" for="flexSwitchCheckDefault">active</label>
						</div>
				    </div>
				    <input id="repeat_end_date" type="date" class="form-control" name="repeat_end_date" style="display: none;"> 
				    {{--<span id="nameHelpBlock" class="form-text text-muted">Max. 100 character</span>--}}
			  </div>

			  <div class="form-group mt-3 repeat" style="display: none;">
			    <label for="select">Repeat Confirmation</label> 
			    <div>
			        <div class="form-check form-switch">
					  <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" name="need_confirmed" value="1">
					  <label class="form-check-label" for="flexSwitchCheckDefault">Active</label>
					</div>
			    </div>
			  </div>

			  <div class="form-group mt-3">
				    <label for="name">Tags</label> 
				    <input id="tags" name="tags" placeholder="Type a tags (exp: #shopping #saving)" type="text" aria-describedby="nameHelpBlock" class="form-control"> 
				    <span id="nameHelpBlock" class="form-text text-muted">split the tags with space</span>
				</div>
			
			  <div class="form-group mt-4">
			    <button name="submit" type="submit" class="btn btn-primary">Save</button>
			  </div>
			</form>

          </div>
        </div>

      </div>
    </div>
</div>
@endsection

@section('script1')
<script type="text/javascript">
	function enabledRepeat(){
		var is_checked= $('#is_repeat').is(":checked");

		if(is_checked){
			$(".repeat").show();
		}else{
			$(".repeat").hide();
		}
	}

	function enabledEndDate(){
		var is_active= $('#repeat_end_definition').is(":checked");

		if(is_active){
			$("#repeat_end_date").show();
		}else{
			$("#repeat_end_date").hide();
		}
	}
</script>
@endsection