@extends('app')
@section('style')
<style type="text/css">
	
</style>
@endsection
@section('content')
<div class="col-xl-12 col-md-12 mt-2">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Basic Info</h4>
        <hr>

        <div class="row">
          <div class="col-md-6">
           	<table class="table" style="border-bottom-width: 0px !important;padding: 0px !important;">
           		<tr>
           			<th style="min-width: 100px;">Title</th>
           			<td style="width: 10px;">:</td>
           			<td>{{$journal->title}}</td>
           		</tr>
           		<tr>
           			<th style="min-width: 100px;">Wallet</th>
           			<td style="width: 10px;">:</td>
           			<td>{{$journal->wallet->name}}</td>
           		</tr>

           		<tr>
           			<th style="min-width: 100px;">Status</th>
           			<td style="width: 10px;">:</td>
           			<td>{{ucfirst($journal->status)}}</td>
           		</tr>

           		<tr>
           			<th style="min-width: 100px;">Description</th>
           			<td style="width: 10px;">:</td>
           			<td>{{$journal->description}}</td>
           		</tr>

           	</table>
          </div>

          <div class="col-md-6">
          	<table class="table" style="border-bottom-width: 0px !important;padding: 0px !important;">
           		<tr>
           			<th>{{ucfirst($journal->status)}} at</th>
           			<td style="width: 10px;">:</td>
           			<td>{{date('d F Y',strtotime($journal->date))}}</td>
           		</tr>

           		<tr>
           			<th>Repeat at</th>
           			<td style="width: 10px;">:</td>
           			<td>
           				@if($journal->is_repeat == '1')
           				{{date('d F Y',strtotime($journal->repeat_next_date))}}
           				@else
           				-
           				@endif
           			</td>
           		</tr>
           	</table>
          </div>
        </div>

      </div>
    </div>

    <div class="card mt-2">
      <div class="card-body">
        <h4 class="card-title">Transaction Info</h4>
        <hr>

        <div class="row">
          <div class="col-md-12">
          	<a href="{{url('journal/'.$journal->id.'/create-transaction')}}" class="btn btn-sm btn-success">
          		<i class="bi bi-cash-coin"></i> Add Transaction
          	</a>
           	<table class="table">

           	  @if($journal->total_amount != 0)
	           	  <!-- income -->
	           	  @if($total_income != 0)
			          <tr>
				            <td colspan="3"><b>Income</b></td>
				            <td nowrap="nowrap" style="text-align: right;"><b>Rp {{number_format($total_income,2, ',' , '.')}}</b></td>
				            <td></td>
				      </tr>
			          
			          @foreach($category_income as $income)
				          <tr>
				          	<td style="width: 30px;"><i class="{{$income->category->icon}}"></i></td>
				          	<td colspan="2">{{$income->category->name}}</td>
				          	<td style="text-align: right;" nowrap="nowrap">Rp {{number_format($income->total_amount,2, ',' , '.')}} </td>
				          	<td style="cursor: pointer;">
				          		<i class="bi-caret-up-fill caret_up_{{$income->id}}" onclick="openTransaction('{{$income->id}}')"></i> <i class="bi-caret-down-fill caret_down_{{$income->id}}" style="display: none;" onclick="closeTransaction('{{$income->id}}')"></i>
				          	</td>
				          </tr>

				          	@php $i=1; @endphp
				          	@foreach ($income->transaction as $transaction)
				          	<tr class="transaction_{{$income->id}}" style="display: none;">
					          	<td></td>
					          	<td style="width: 30px;font-size: 14px;">{{$i}}.</td>
					          	<td style="font-size: 14px;">
					          	    {{$transaction->note}}  
					          	    
					          	    @if($transaction->total_item)
					          	    <br>   {{'@'.@$transaction->unit_price}} X {{@$transaction->total_item}}
					          	    @endif
					          	</td>
					          	
					          	<td style="font-size: 14px;text-align: right;" nowrap="nowrap">Rp {{number_format($transaction->amount,2, ',' , '.')}}</td>
					          	<td nowrap="nowrap">
					          		<span style="color: red;cursor: pointer;" title="delete transaction"  data-bs-toggle="modal" data-bs-target="#exampleModal_{{$transaction->id}}"><i class="bi-trash-fill"></i></span>
					          		{{--<span style="color: green;cursor: pointer;margin-left: 10px;" title="edit transaction"><i class="bi-pencil-square"></i></span>--}}
					          	</td>
					        </tr>

					        <!-- Modal -->
							<div class="modal fade" id="exampleModal_{{$transaction->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Delete Transaction</h5>
							        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
							      </div>
							      <div class="modal-body">
							        Are you sure to delete <b>{{$transaction->note}}</b> from your journal: <b>{{$journal->title}}</b>?
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
							        <a href="{{url('journal/'.$journal->id.'/delete-transaction/'.$transaction->id)}}">
							        	<button type="button" class="btn btn-primary">Yes</button>
							        </a>
							      </div>
							    </div>
							  </div>
							</div>

					        @php $i++; @endphp
				          	@endforeach
				      @endforeach

			      @endif
			      <!-- end income -->
			      
			      <!-- ar -->
		          @if($total_ar != 0)
			          <tr>
			            <td colspan="3"><b>Account Receivable</b></td>
			            <td nowrap="nowrap" style="text-align: right;"><b>Rp {{number_format($total_ar,2, ',' , '.')}}</b></td>
			            <td></td>
			          </tr>

			          @foreach($category_ar as $ar)
			          <tr>
			          	<td style="width: 30px;"><i class="{{$ar->category->icon}}"></i></td>
			          	<td colspan="2">{{$ar->category->name}}</td>
			          	<td style="text-align: right;" nowrap="nowrap">Rp {{number_format($ar->total_amount,2, ',' , '.')}} </td>
			          	<td style="cursor: pointer;">
			          		<i class="bi-caret-up-fill caret_up_{{$ar->id}}" onclick="openTransaction('{{$ar->id}}')"></i> <i class="bi-caret-down-fill caret_down_{{$ar->id}}" style="display: none;" onclick="closeTransaction('{{$ar->id}}')"></i>
			          	</td>
			          </tr>

			          	@php $i=1; @endphp
			          	@foreach ($ar->transaction as $transaction)
			          	<tr class="transaction_{{$ar->id}}" style="display: none;">
				          	<td></td>
				          	<td style="width: 30px;font-size: 14px;">{{$i}}.</td>
				          	<td style="font-size: 14px;">
				          	    {{@$transaction->note}} 

				          	  
				          	</td>
				          	<td style="font-size: 14px;text-align: right;" nowrap="nowrap">Rp {{number_format($transaction->amount,2, ',' , '.')}}</td>
				          	<td nowrap="nowrap">
				          		<a href="{{url('ar/paid/payment/history/'.$transaction->id)}}">
				          			<span style="color: blue;cursor: pointer;"><i class="bi-info-circle-fill"></i></span>
				          		</a>
				          		

				          		&nbsp;&nbsp;

				          		<span style="color: red;cursor: pointer;" title="delete transaction"  data-bs-toggle="modal" data-bs-target="#exampleModal_{{$transaction->id}}"><i class="bi-trash-fill"></i></span>


				          		{{--<span style="color: green;cursor: pointer;margin-left: 10px;" title="edit transaction"><i class="bi-pencil-square"></i></span>--}}
				          	</td>
				        </tr>

				        <!-- Modal -->

								<div class="modal fade" id="exampleModal_{{$transaction->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
								  <div class="modal-dialog">
								    <div class="modal-content">
								      <div class="modal-header">
								        <h5 class="modal-title" id="exampleModalLabel">Delete Transaction</h5>
								        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
								      </div>
								      <div class="modal-body">
								        Are you sure to delete <b>{{@$transaction->item->name}}</b> from your journal: <b>{{$journal->title}}</b>?
								      </div>
								      <div class="modal-footer">
								        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
								        <a href="{{url('journal/'.$journal->id.'/delete-transaction/'.$transaction->id)}}">
								        	<button type="button" class="btn btn-primary">Yes</button>
								        </a>
								      </div>
								    </div>
								  </div>
								</div>

				        @php $i++; @endphp
			          	@endforeach
			          @endforeach
		          @endif
		          <!-- end ar -->

			      <!-- expanditure -->
		          @if($total_expanditure != 0)
			          <tr>
			            <td colspan="3"><b>Expanditure</b></td>
			            <td nowrap="nowrap" style="text-align: right;"><b>Rp {{number_format($total_expanditure,2, ',' , '.')}}</b></td>
			            <td></td>
			          </tr>

			          @foreach($category_expanditure as $expanditure)
			          <tr>
			          	<td style="width: 30px;"><i class="{{$expanditure->category->icon}}"></i></td>
			          	<td colspan="2">{{$expanditure->category->name}}</td>
			          	<td style="text-align: right;" nowrap="nowrap">Rp {{number_format($expanditure->total_amount,2, ',' , '.')}} </td>
			          	<td style="cursor: pointer;">
			          		<i class="bi-caret-up-fill caret_up_{{$expanditure->id}}" onclick="openTransaction('{{$expanditure->id}}')"></i> <i class="bi-caret-down-fill caret_down_{{$expanditure->id}}" style="display: none;" onclick="closeTransaction('{{$expanditure->id}}')"></i>
			          	</td>
			          </tr>

			          	@php $i=1; @endphp
			          	@foreach ($expanditure->transaction as $transaction)
			          	<tr class="transaction_{{$expanditure->id}}" style="display: none;">
				          	<td></td>
				          	<td style="width: 30px;font-size: 14px;">{{$i}}.</td>
				          	<td style="font-size: 14px;">
				          			@if(@$transaction->item->name)
				          	    {{@$transaction->item->category->name}} - {{@$transaction->item->name}} 
				          	    @else
				          	    {{@$transaction->note}} 
				          	    @endif
				          	    
				          	    @if($transaction->item)
				          	    <br> <i>Netto: {{@$transaction->item->total}} {{@$transaction->item->unit->name}}</i> 
				          	    @endif
				          	    @if($transaction->total_item)
					          	    <br>   {{'@'.@$transaction->unit_price}} X {{@$transaction->total_item}}
					          	@endif
				          	</td>
				          	<td style="font-size: 14px;text-align: right;" nowrap="nowrap">Rp {{number_format($transaction->amount,2, ',' , '.')}}</td>
				          	<td nowrap="nowrap">
				          		<span style="color: red;cursor: pointer;" title="delete transaction"  data-bs-toggle="modal" data-bs-target="#exampleModal_{{$transaction->id}}"><i class="bi-trash-fill"></i></span>
				          		{{--<span style="color: green;cursor: pointer;margin-left: 10px;" title="edit transaction"><i class="bi-pencil-square"></i></span>--}}
				          	</td>
				        </tr>

				        <!-- Modal -->
						<div class="modal fade" id="exampleModal_{{$transaction->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">Delete Transaction</h5>
						        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						      </div>
						      <div class="modal-body">
						        Are you sure to delete <b>{{@$transaction->item->name}}</b> with netto: <b>{{@$transaction->item->total}} {{@$transaction->item->unit->name}}</b> from your journal: <b>{{$journal->title}}</b>?
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
						        <a href="{{url('journal/'.$journal->id.'/delete-transaction/'.$transaction->id)}}">
						        	<button type="button" class="btn btn-primary">Yes</button>
						        </a>
						      </div>
						    </div>
						  </div>
						</div>

				        @php $i++; @endphp
			          	@endforeach
			          @endforeach
		          @endif
		          <!-- end expanditure -->
		          
		          @if(count($category_saving) > 0)
		          <tr>
			            <td colspan="3"><b>Saving</b></td>
			            <td nowrap="nowrap" style="text-align: right;"><b>Rp {{number_format($total_saving,2, ',' , '.')}}</b></td>
			            <td></td>
			      </tr>
		       

		            @foreach($category_saving as $saving)
			          <tr>
			          	<td style="width: 30px;"><i class="{{$saving->category->icon}}"></i></td>
			          	<td colspan="2">{{$saving->category->name}}</td>
			          	<td style="text-align: right;" nowrap="nowrap">Rp {{number_format($saving->total_amount,2, ',' , '.')}} </td>
			          	<td style="cursor: pointer;">
			          		<i class="bi-caret-up-fill caret_up_{{$saving->id}}" onclick="openTransaction('{{$saving->id}}')"></i> <i class="bi-caret-down-fill caret_down_{{$saving->id}}" style="display: none;" onclick="closeTransaction('{{$saving->id}}')"></i>
			          	</td>
			          </tr>

			          	@php $i=1; @endphp
			          	@foreach ($saving->transaction as $transaction)
			          	<tr class="transaction_{{$saving->id}}" style="display: none;">
				          	<td></td>
				          	<td style="width: 30px;font-size: 14px;">{{$i}}.</td>
				          	<td style="font-size: 14px;">
				          	    {{$transaction->note}} 
				          	    @if($transaction->total_item)
					          	    <br>   {{'@'.@$transaction->unit_price}} X {{@$transaction->total_item}}
					          	@endif
				          	</td>
				          	
				          	<td style="font-size: 14px;text-align: right;" nowrap="nowrap">Rp {{number_format($transaction->amount,2, ',' , '.')}}</td>
				          	<td nowrap="nowrap">
				          		<span style="color: red;cursor: pointer;" title="delete transaction"  data-bs-toggle="modal" data-bs-target="#exampleModal_{{$transaction->id}}"><i class="bi-trash-fill"></i></span>
				          		{{--<span style="color: green;cursor: pointer;margin-left: 10px;" title="edit transaction"><i class="bi-pencil-square"></i></span>--}}
				          	</td>
				        </tr>

				        <!-- Modal -->
						<div class="modal fade" id="exampleModal_{{$transaction->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">Delete Transaction</h5>
						        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						      </div>
						      <div class="modal-body">
						        Are you sure to delete <b>{{$transaction->note}}</b> from your journal: <b>{{$journal->title}}</b>?
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
						        <a href="{{url('journal/'.$journal->id.'/delete-transaction/'.$transaction->id)}}">
						        	<button type="button" class="btn btn-primary">Yes</button>
						        </a>
						      </div>
						    </div>
						  </div>
						</div>

				        @php $i++; @endphp
			          	@endforeach
			      @endforeach
		             @endif

		          <tr style="background: #df744a;color: white;">
		            <th colspan="3">Total</th>

		            <td style="text-align: right;">
		            	<b>Rp {{number_format(str_replace("-","",$journal->total_amount),2, ',' , '.')}}</b>
		            </td>

		            <td></td>
		          </tr>

	          @else
	          <tr>
	          	<td>
	          		<center>
	          			Transaction is not found
	          		</center>
	          	</td>
	          </tr>
	          @endif
	        </table>
          </div>
          </div>
        </div>

      </div>
    </div>

    
</div>
@endsection

@section('script1')
<script type="text/javascript">
	function openTransaction(id){
		$(".transaction_"+id).show();
		$(".caret_up_"+id).hide();
		$(".caret_down_"+id).show();
	}

	function closeTransaction(id){
		$(".transaction_"+id).hide();
		$(".caret_up_"+id).show();
		$(".caret_down_"+id).hide();
	}

</script>
@endsection