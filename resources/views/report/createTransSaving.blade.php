              @if($cat_id == "30")
              <div class="form-group mt-3">
                  <label for="name">Item</label> 
                  <input type="hidden" id="item_id" name="item_id">
                  <input id="item_name" name="item_name" placeholder="Select Item" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control" autocomplete="off" onclick="openItem()"> 
              </div>

              <div class="form-group mt-3">
                  <label for="name">Total Item</label> 
                  <input id="total_item" name="total_item" placeholder="Type total item" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control" onkeyup="calculate('total_item')"> 
              </div>


              <div class="form-group mt-3">
                  <label for="name">Price Per Unit</label> 
                  <input id="unit_price" name="unit_price" placeholder="Type the price" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control" onkeyup="calculate('unit_price')"> 
              </div>

              <div class="form-group mt-3">
                  <label for="name">Shipping Cost</label> 
                  <input id="shipping_cost" name="shipping_cost" placeholder="Type shipping cost" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control" value="0" onkeyup="calculate('shipping_cost')"> 
              </div>

              <div class="form-group mt-3">
                  <label for="name">Total Price</label> 
                  <input id="total_price" name="total_price" placeholder="Type total price" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control" readonly onkeyup="calculate('total_price')"> 
              </div>
              @else
              
              <div class="form-group mt-3">
                  <label for="name">Amount</label> 
                  <input id="total_price" name="total_price" placeholder="Type amount" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control"> 
              </div>
              @endif

              <div class="form-group mt-3">
                  <label for="name">Saving Goals</label> 
                  <input id="note" name="note" placeholder="Type your saving goals" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control"> 
                  {{--<span id="nameHelpBlock" class="form-text text-muted">Max. 100 character</span>--}}
              </div>


              <div class="form-group mt-3 repeat">
                <label for="select">Select your saving wallet</label> 
                <div>
                  @if(count($wallets) > 1)
                  <select name="wallet_id" id="wallet_id" class="form-control js-example-basic-single" style="width: 100%;">
                    @foreach($wallets as $wallet)
                    <option value="{{$wallet->id}}">{{$wallet->name}}</option>
                    @endforeach
                  </select>
                  @elseif(count($wallets) == 1)
                    @foreach($wallets as $wallet)
                      <input type="hidden" name="wallet_id" value="{{$wallet->id}}">
                      <input type="text" name="wallet_name" class="form-control" value="{{$wallet->name}}" readonly>
                    @endforeach
                  @else
                    <div style="border: 1px dashed black;">
                      <center>You don't have a wallet to saving. <a href="#">check here</a></center>
                    </div>
                  @endif
                </div>
              </div>






