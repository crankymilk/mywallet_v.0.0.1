              
              <div class="form-group mt-3">
                  <label for="name">Description</label> 
                  <input type="hidden" id="item_id" name="item_id">
                  <input id="note" name="note" placeholder="Type a note" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control" autocomplete="off"> 
              </div>

              <div class="form-group mt-3">
                  <label for="name">Receiver name</label> 
                  <input type="hidden" id="item_id" name="item_id">
                  <input id="receiver_name" name="receiver_name" placeholder="Type a receiver name" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control" autocomplete="off"> 
              </div>

              <div class="form-group mt-3">
                  <label for="name">Receiver phone</label> 
                  <input type="hidden" id="item_id" name="item_id">
                  <input id="receiver_phone" name="receiver_phone" placeholder="Type a receiver phone" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control" autocomplete="off"> 
              </div>

              <div class="form-group mt-3">
                  <label for="name">Receiver email</label> 
                  <input type="hidden" id="item_id" name="item_id">
                  <input id="receiver_email" name="receiver_email" placeholder="Type a receiver email" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control" autocomplete="off"> 
              </div>

              <div class="form-group mt-3">
                  <label for="name">Amount</label> 
                  <input id="total_price" name="total_price" placeholder="Type amount" type="text" aria-describedby="nameHelpBlock" required="required" class="form-control"> 
              </div>

              <div class="form-group mt-3 repeat">
                <label for="select">Payment Method</label> 
                <div>
                    <div class="form-check form-switch">
                  <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" name="payment_method" value="unlimited">
                  <label class="form-check-label" for="flexSwitchCheckDefault">Unlimited</label>
                </div>
                </div>
              </div>

            <div class="form-group mt-3 limited">
                <label for="name">Payment Start Date</label> 
                <input id="start_date" name="start_date" type="date" class="form-control"> 
            </div>

            <div class="form-group mt-3 limited">
              <label for="select">Payment Duration</label> 
              <div>
                <select id="repeat_duration" name="repeat_duration"  class="form-control">
                    <option value="yearly">yearly</option>
                    <option value="monthly">Monthly</option>
                    <option value="weekly">Weekly</option>
                    <option value="daily">Daily</option>
                </select>
              </div>
            </div>

            <div class="form-group mt-3">
                  <label for="name">Total Credit</label> 
                  <input id="credit" name="credit" placeholder="Type amount" type="number" aria-describedby="nameHelpBlock" class="form-control" value="1"> 
              </div>