@extends('app')
@section('content')
	<div class="row">
	    <div class="col-xl-12 col-md-12 mt-2">
		    <div class="card">
		      <div class="card-body">
		        <h4 class="card-title">Need Confirmed</h4>
		        

		        <div class="row">

		          @if(count($bills) > 0 OR count($bills_with_enddate) > 0)
		            @foreach($bills as $bill)
		              <div class="col-md-4 mt-2" style="margin-top: 1.5%;">
		                <table>
		                  <tr>
		                    <td>
		                      <i class="bi-gear-fill" style="font-size: 1rem;"></i>
		                    </td>
		                    <td>
		                      {{$bill->title}}
		                    </td>
		                  </tr>
		                  <tr>
		                    <td></td>
		                    <td>
		                      @php
		                        if($bill->repeat_duration == 'monthly'){
		                          $a = ' months';
		                        }else if($bill->repeat_duration == 'weekly'){
		                          $a = ' weeks';
		                        }else if($bill->repeat_duration == 'daily'){
		                          $a = ' days';
		                        }

		                        if($bill->repeat_duration_times > 1){
		                          $duration = 'Every '.$bill->repeat_duration_times.$a;
		                        }
		                        else{
		                          $duration = ucfirst($bill->repeat_duration);
		                        }
		                      @endphp

		                      Duration: {{$duration}}

		                      <br>
		                      Due Date: {{date('F d, Y',strtotime($bill->repeat_next_date))}}
		                      <br>

		                      @php
		                      if($bill->merge_times > 0){
		                      	$total = $bill->total_amount * ($bill->merge_times + 1);
		                  	  }else{
		                  	  	$total = $bill->total_amount;
		                  	  }
		                      @endphp

		                      Rp {{number_format($total,2, ',' , '.')}}


		                      @if($bill->merge_times > 0)
		                        (merge {{$bill->merge_times}} times)
		                      @endif
		                      <br>
		                      
		                      <br>
		                      <br>

		                       <a href="{{url('bill/merge/'.$bill->id)}}">
		                         <button class="btn btn-warning">Merge</button>
		                       </a>

		                       <a href="{{url('bill/paid/'.$bill->id)}}">
		                         <button class="btn btn-success">Paid</button>
		                       </a>
		                    </td>
		                  </tr>
		                </table>
		              </div>
		            @endforeach

		            @foreach($bills_with_enddate as $bill)
		              <div class="col-md-4 mt-2" style="margin-top: 1.5%;">
		                <table>
		                  <tr>
		                    <td>
		                      <i class="bi-gear-fill" style="font-size: 1rem;"></i>
		                    </td>
		                    <td>
		                      {{$bill->title}}
		                    </td>
		                  </tr>
		                  <tr>
		                    <td></td>
		                    <td>
		                      @php
		                        if($bill->repeat_duration == 'monthly'){
		                          $a = ' months';
		                        }else if($bill->repeat_duration == 'weekly'){
		                          $a = ' weeks';
		                        }else if($bill->repeat_duration == 'daily'){
		                          $a = ' days';
		                        }

		                        if($bill->repeat_duration_times > 1){
		                          $duration = 'Every '.$bill->repeat_duration_times.$a;
		                        }
		                        else{
		                          $duration = ucfirst($bill->repeat_duration);
		                        }
		                      @endphp

		                      Duration: {{$duration}}

		                      <br>
		                      Due Date: {{date('F d, Y',strtotime($bill->repeat_next_date))}}
		                      <br>

		                      @php
		                      if($bill->merge_times > 0){
		                      	$total = $bill->total_amount * ($bill->merge_times + 1);
		                  	  }else{
		                  	  	$total = $bill->total_amount;
		                  	  }
		                      @endphp

		                      Rp {{number_format($total,2, ',' , '.')}}


		                      @if($bill->merge_times > 0)
		                        (merge {{$bill->merge_times}} times)
		                      @endif
		                      <br>
		                      
		                      <br>
		                      <br>

		                       <a href="{{url('bill/merge/'.$bill->id)}}">
		                         <button class="btn btn-warning">Merge</button>
		                       </a>

		                       <a href="{{url('bill/paid/'.$bill->id)}}">
		                         <button class="btn btn-success">Paid</button>
		                       </a>
		                    </td>
		                  </tr>
		                </table>
		              </div>
		            @endforeach
		          @else
		            <center>
		              <h5>
		                <i class="fa fa-check-circle" style="font-size: 4rem;color:green;"></i> <br><br>
		                All bill is confirmed
		              </h5>
		            </center>
		          @endif

		        </div>

		    
		      </div>
		    </div>
		</div>

		<div class="col-xl-12 col-md-12 mt-2">
		    <div class="card">
		      <div class="card-body">
		        <h4 class="card-title">Merge Bill</h4>
		        

		        <div class="row">

		          @if(count($mergebills) > 0)
		            @foreach($mergebills as $bill)
		              <div class="col-md-6 mt-2" style="margin-top: 1.5%;">
		                <table>
		                  <tr>
		                    <td>
		                      <i class="bi-gear-fill" style="font-size: 1rem;"></i>
		                    </td>
		                    <td>
		                      {{$bill->title}}
		                    </td>
		                  </tr>
		                  <tr>
		                    <td></td>
		                    <td>
		                      @php
		                        if($bill->repeat_duration == 'monthly'){
		                          $a = ' months';
		                        }else if($bill->repeat_duration == 'weekly'){
		                          $a = ' weeks';
		                        }else if($bill->repeat_duration == 'daily'){
		                          $a = ' days';
		                        }

		                        if($bill->repeat_duration_times > 1){
		                          $duration = 'Every '.$bill->repeat_duration_times.$a;
		                        }
		                        else{
		                          $duration = ucfirst($bill->repeat_duration);
		                        }
		                      @endphp

		                      Duration: {{$duration}}

		                      <br>
		                      Due Date: {{date('F d, Y',strtotime($bill->repeat_next_date))}}
		                      <br>
		                      @php
		                      if($bill->merge_times > 0){
		                      	$total = $bill->total_amount * ($bill->merge_times + 1);
		                  	  }else{
		                  	  	$total = $bill->total_amount;
		                  	  }
		                      @endphp

		                      Rp {{number_format($total,2, ',' , '.')}}

		                      @if($bill->merge_times > 0)
		                        (merge {{$bill->merge_times}} times)
		                      @endif
		                      
		                    </td>
		                  </tr>
		                </table>
		              </div>
		            @endforeach
		          @else
		            <center>
		              <h5>
		                <i class="fa fa-check-circle" style="font-size: 4rem;color:green;"></i> <br><br>
		                All bill is confirmed
		              </h5>
		            </center>
		          @endif

		        </div>

		      </div>
		    </div>
		</div>
	</div>

@endsection