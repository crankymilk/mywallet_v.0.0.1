<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link href="{{url('assets/fontawesome')}}/css/all.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <title>My Wallet</title>

    <style type="text/css">
      #category-option{
        border: 1px solid black;
        min-height: 500px;
        position: absolute;
      }

      .select2-container .select2-selection--single {
        height: 38px !important;
      }

      .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 36px !important;
      }

      .select2-container--default .select2-selection--single {
          border: 1px solid #aaaaaa80 !important;
          border-radius: 6px !important;
      }
    </style>

    @yield('style')
  </head>
  <body>
    <nav class="navbar navbar-expand-md fixed-top" style="background: #356866;">
      <div class="container-fluid">
        <a class="navbar-brand" href="#" style="color: white;font-size: 2rem;">
          <i class="bi-wallet-fill"></i> &nbsp;My Wallet
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        
      </div>
    </nav>

   
    <div class="container" style="margin-bottom: 10%;">

      @yield('content')
    </div>

    <footer class="text-center text-white fixed-bottom" style="background-color: #356866;">
      @yield('modalBottom1')
      @yield('modalBottom2')

      <div class="row">
        @php
          $url_dashboard = url('dashboard');
          $url_create_journal = url('journal/create');
        @endphp
        <div class="col-4" style="cursor: pointer;" onclick="redirect('{{$url_dashboard}}')">
          <i class="bi-grid" style="font-size: 2rem;"></i>
        </div>
         <div class="col-4" style="cursor: pointer;" onclick="redirect('{{$url_create_journal}}')">
          <i class="bi-file-earmark-plus-fill" style="font-size: 2rem;"></i>
        </div>
         <div class="col-4" style="cursor: pointer;">
          <i class="bi-person-circle" style="font-size: 2rem;"></i>
        </div>
      </div>
    </footer>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        var height_header = $("nav").height() + 30;

        $(".container").css("margin-top",height_header+"px");

        $('.js-example-basic-single').select2();

      });

      function redirect(url){
        $(location).prop('href', url);
      }
    </script>

    @yield('script1')
    @yield('script2')
    @yield('scriptCreateExpanditure')
  </body>
</html>