            
            <table class="table">
              @foreach($items as $item)
              <tr onclick="chooseItem('{{$item->id}}','{{$item->name}} (Netto: {{$item->total}} {{$item->unit->name}})')">
                <td><b>{{$item->name}}</b> <br>  Netto: {{$item->total}} {{$item->unit->name}}</td>
              </tr>
              @endforeach
              <tr class="create-new-item" style="border: 2px dashed black;cursor: pointer;" onclick="createNewItem()">
                <td>
                  <center><h5>+ Add New</h5></center>
                </td>
              </tr>
            </table>

            <table class="table store-new-item" style="display: none;">
              <tr>
                <td colspan="2">
                  <input type="text" name="name" id="name" placeholder="Type item name" class="form-control" autocomplete="off">
                </td>
              </tr>
              <tr>
                <td style="width: 50px;">
                  <input type="number" name="total" id="total" placeholder="Type netto number" class="form-control" autocomplete="off">
                </td>
            
                <td style="width: 100px;">
                  <select name="unit_id" id="unit_id" class="form-control js-example-basic-single" style="width: 100%;">
                    <option hidden disabled selected>Select Unit Price</option>
                    @foreach($units as $unit)
                    <option value="{{$unit->id}}">{{$unit->name}} <br> <i>({{$unit->translate}})</i></option>
                    @endforeach
                  </select>
                </td>
              </tr>
              <tr>
                <td>
                  <button class="btn btn-sm btn-danger" onclick="cancelNewItem()">Cancel</button>
                  <button class="btn btn-sm btn-success" onclick="saveNewItem()">Save</button>
                </td>
              </tr>
            </table>

@section('script2')
<script type="text/javascript">

  

  function createNewItem()
  {
    $('.store-new-item').show();
    $('.create-new-item').hide();
  }

  function cancelNewItem()
  {
    $('.create-new-item').show();
    $('.store-new-item').hide();
  }

  function saveNewItem(){
    var name = $("#name").val();
    var netto = $("#total").val();
    var unit = $("#unit_id").val();
    var url = "{{url('ajax/item/store')}}";
    var token = "{{csrf_token()}}";

    $.ajax({
      type: "GET",
      url:url,
      data: {name: name,netto:netto,unit:unit,_token:token},
      success: function(data) {
        $("#items-list").html(data.html);
        cancelNewItem();
      },
      error: function(data) {
                successmessage = 'Error';
      },    
    });
  }

  function chooseItem(item_id,item_name){
    $("#item_id").val(item_id);
    $("#item_name").val(item_name);

    closeItem();
  }
</script>
@endsection