@foreach($wallets as $wallet)
<tr>
  <td>{{$wallet->name}}</td>
  <td>
      <div>
	  <div class="form-check form-switch">
		<input class="form-check-input" type="checkbox" id="activation_wallet_{{$wallet->id}}" name="activation_wallet" value="{{$wallet->id}}" {{$wallet->is_active == '1' ? 'checked' : ''}}>
	  </div>
	</div>
  </td>
</tr>
@endforeach