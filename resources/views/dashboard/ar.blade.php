<div class="col-xl-4 col-md-12 mt-2">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Account Receivable</h4>
        

        <div class="row">
          @if(count($piutang) > 0)
          @foreach($piutang as $value)
          <div class="col-md-12" style="margin-top: 2%;">
            <table>
              <tr>
                <td>
                  <i class="bi-gear-fill" style="font-size: 1rem;"></i>
                </td>
                <td>
                  {{$value->ar->connection->name}} - <i>{{$value->transaction->note}}</i>
                </td>
              </tr>
              <tr>
                <td></td>
                <td>
                  Total Merge: {{$value->merge}} times
                  <br>
                  Due Date: {{date('j F Y',strtotime($value->duedate))}}
                  <br>
                  <br>

                  Total: Rp {{number_format($value->ar->total_ar,2, ',' , '.')}}
                  <br>
                  Paid: Rp {{number_format($value->paid,2, ',' , '.')}}
                  <br>
                  Balancing: Rp {{number_format($value->balancing,2, ',' , '.')}}
                  <br>
                  Billed: Rp {{number_format($value->amount,2, ',' , '.')}} <br>
                  
                  <br>
                  <br>

                   @if($value->number == $value->ar->total_bill)

                   <a href="{{url('ar/postponed/'.$value->id.'/'.$value->amount)}}" style="text-decoration: none;" onclick="return confirm('Are you sure this account receivable is posponed?')">
                     <button class="btn btn-danger">postponed</button>
                   </a> 
                   @else
                   <a href="{{url('ar/merge/'.$value->id.'/'.$value->amount)}}" style="text-decoration: none;" onclick="return confirm('Are you sure this account receivable is merged?')">
                     <button class="btn btn-warning">Merge</button>
                   </a>
                   @endif


                   <a href="{{url('ar/paid/'.$value->id.'/'.$value->amount)}}" style="text-decoration: none;" onclick="return confirm('Are you sure this account receivable is paid?')">
                     <button class="btn btn-success">Paid</button>
                   </a>
                </td>
              </tr>
            </table>
          </div>
          @endforeach
          @else
            <center>
              <h5>
                <i class="fa fa-check-circle" style="font-size: 4rem;color:green;"></i> <br><br>
                All account receivable is confirmed to be paid or merge
              </h5>
            </center>
          @endif
        </div>

        {{-- <hr>
       <center>
          <a href="#" >Show more >></a>
        </center>--}}
      </div>
    </div>
</div>