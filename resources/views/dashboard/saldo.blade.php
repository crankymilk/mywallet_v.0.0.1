
	<div class="col-xl-3 col-md-12 mt-2">
		<div class="card text-center" style="background: #07302f;">
		  <div class="card-body">
		    <h5 class="card-title">
		    	<img src="{{url('assets/no_profil.png')}}" class="rounded-circle" style="width: 40%;">
		    </h5>
		    <p class="card-text" style="color: white;">Hi, <b>{{Auth::user()->name}}</b> <br> Your saldo:</p>

		    <h3 style="color:#df744a !important;">Rp {{number_format($balancing_wallet_active,2, ',' , '.')}}</h3>

		    <br>

		    @foreach($wallets as $wallet)
		    @if($wallet->is_active == '1')
		    <span class="btn btn-outline-primary">{{$wallet->name}}</span>
		    @endif
		    @endforeach
		    <span class="btn btn-outline-primary"><i class="bi-gear-fill" onclick="setWallet()"></i></span>
		  </div>
		</div>
	</div>

@section('modalBottom2')
      <!-- item option -->
      <div class="row wallet-option" style="background: #e3e3e3a6;text-align: left;display: none;">
        <div class="col-12">
          <table class="table">
            <tr> 
              <td>
                <h3>Wallets</h3>

              </td>
              <td style="text-align: right;">
                <button class="btn btn-danger btn-sm" onclick="closeSetWallet()">Close</button>
              </td>
            </tr>
          </table>
         

          <div id="items-list" style="overflow:auto;max-height: 400px;">
          	<table class="table wallets">
          		@include('wallet.index')
          	</table>
            <table class="table"> 
            	<tr>
            		<td colspan="2">
            			<div id="btn-add-wallet" style="border: 1px dashed black;cursor:pointer;" onclick="createWallet()">
            				<center>
            					<h6>+ Add Wallet</h6>
            				</center>
            			</div>

            			<div id="create_wallet" style="display: none;">
            				<div class="form-group mt-3 custom_days">
				                  <label for="name">Wallet Name</label> 
				                  <input id="name" name="name" type="text"required="required" class="form-control" value="" placeholder="Type wallet name" autocomplete="off">
				             </div>



			                <div class="form-group mt-3 option_custom_days">
			                    <label for="name">Is this wallet your saving?</label> 
			                    <div>
			                        <div class="form-check form-switch">
			                      <input class="form-check-input" type="checkbox" id="is_saving" name="is_saving" value="1">
			                      <label class="form-check-label" for="flexSwitchCheckDefault">Yes</label>
			                    </div>
			                    </div>
			                </div>

			                <div class="form-group mt-4">
				                <button type="button" class="btn btn-danger" onclick="cancelWallet()">Cancel</button>
				                <button name="button" type="submit" class="btn btn-primary" onclick="storeWallet()">Save</button>
				            </div>
            			</div>
            		</td>
            	</tr>
            </table>
          </div>
          
        </div>
      </div>
@endsection

@section('script1')
<script type="text/javascript">
	function activationWallet(wallet_id){
		var is_checked= $('#activation_wallet_'+wallet_id).is(":checked");

		if(is_checked){
			$(location).prop('href', "{{url('wallet/active')}}/"+wallet_id);
		}else{
			$(location).prop('href', "{{url('wallet/inactive')}}/"+wallet_id);
		}
	}

	function setWallet()
	{
		$(".wallet-option").toggle();
	}

	function closeSetWallet()
	{
		$(".wallet-option").hide();
		updateWallet();
	}

	function createWallet()
	{
		$("#create_wallet").show();
		$("#btn-add-wallet").hide();
		$(".wallets").hide();
	}

	function cancelWallet()
	{
		$(".wallets").show();
		$("#create_wallet").hide();
		$("#btn-add-wallet").show();
	}

	function storeWallet()
	{
		var is_saving = $("#is_saving").val();
		var name = $("#name").val();
		var url = "{{url('ajax/wallet/store')}}";

		$.ajax({
			type: "GET",
			url: url,
			data: {is_saving:is_saving, name: name},
			success: function(data){
				$(".wallets").html(data.html);
				$("#is_saving").val(""); 
				$("#name").val("");
				cancelWallet();
			},
		      error: function(data) {
		                successmessage = 'Error';
		      },    
	    });
	}

	function updateWallet()
	{
		var wallet_id = new Array();
		$("input[name=activation_wallet]:checked").each(function () {
                wallet_id.push(this.value);
        });
	
		var url = "{{url('ajax/wallet/update')}}";

		$.ajax({
			type: "GET",
			url: url,
			data: {wallet_id:wallet_id},
			success: function(data){
				$(location).prop('href', "{{url('dashboard')}}");
			},
		      error: function(data) {
		                successmessage = 'Error';
		      }
	    });
	}

</script>
@endsection