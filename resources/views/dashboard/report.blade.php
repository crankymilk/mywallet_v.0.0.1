<div class="col-xl-12 col-md-12 mt-2">
    <div class="card">
      <h5 class="card-header">
        @php
            $prev_start_date = date('Y-m-d', strtotime($start_date." -1 month"));
            $prev_end_date = date('Y-m-t', strtotime($prev_start_date));
        @endphp
        <a href="{{url('dashboard/'.$prev_start_date.'/'.$prev_end_date)}}">
        <span style="color:blue;cursor: pointer;"><i class="bi-arrow-left-square-fill"></i></span>
        </a>
        
          Journal <br>{{date('j F Y',strtotime($start_date))}} - {{date('j F Y',strtotime($end_date))}} 
        <span style="color:blue;cursor: pointer;"><i class="bi-gear-fill" onclick="setDate()"></i></span>
      </h5>


      <div class="card-body">
        

        <table class="table">
          <tr style="background: #df744a;color: white;">
            <th colspan="2">Beginning Balance</th>
            <td style="text-align: right;">Rp {{number_format($beginning_balance,2, ',' , '.')}}</td>
            <td></td>
          </tr>

          <tr>
            <th colspan="2">Income <br> </th>
            <td style="text-align: right;font-weight: bold;">Rp {{number_format($total_income,2, ',' , '.')}}</td>
            <td></td>
          </tr>

          @foreach($category_income as $income)
          <tr>
            <td style="width: 30px;"></td>
            <td style="font-size: 14px;"><span style="font-size: 10px;color:grey;">{{date('j F Y',strtotime($income->date))}}</span><br>{{$income->title}}</td>
            <td style="text-align: right;font-size: 14px;">
              Rp {{number_format($income->journal_transaction,2, ',' , '.')}}
            </td>
            <td style="font-size: 14px;">
              <a href="{{url('journal/'.$income->journal_id)}}" style="text-decoration: none;">
               <i class="bi-info-circle-fill" style="font-size: 1.2rem;"></i> 
              </a>

              &nbsp;&nbsp;&nbsp;
              <a href="{{url('journal/delete/'.$income->journal_id)}}" style="text-decoration: none;color: red;" onclick="return confirm('Are you sure want to delete {{$income->title}}?')">
               <i class="bi-trash-fill" style="font-size: 1.2rem;"></i> 
              </a>
            </td>
          </tr>
          @endforeach

          <tr>
            <th colspan="2">Account Receivable</th>
            <td style="text-align: right;font-weight: bold;" nowrap="nowrap">Rp {{number_format(str_replace("-","",$total_ar),2, ',' , '.')}}</td>
            <td>
            </td>
          </tr>

          @foreach($category_ar as $ar)
          <tr>
            <td style="width: 30px;"></td>
            <td style="font-size: 14px;"><span style="font-size: 10px;color:grey;">{{date('j F Y',strtotime($ar->date))}}</span><br>{{substr($ar->title,0,30)}}</td>
            <td style="text-align: right;font-size: 14px;" nowrap="nowrap">
              Rp {{number_format(str_replace("-","",$ar->journal_transaction),2, ',' , '.')}}
            </td>
            <td nowrap="nowrap" style="font-size: 14px;">
              <a href="{{url('journal/'.$ar->journal_id)}}" style="text-decoration: none;">
               <i class="bi-info-circle-fill" style="font-size: 1.2rem;"></i> 
              </a>
              &nbsp;&nbsp;&nbsp;
              <a href="{{url('journal/delete/'.$ar->journal_id)}}" style="text-decoration: none;color: red;" onclick="return confirm('Are you sure want to delete {{$ar->title}}?')">
               <i class="bi-trash-fill" style="font-size: 1.2rem;"></i> 
              </a>
            </td>
          </tr>
          @endforeach


          <tr>
            <th colspan="2">Expanditure</th>
            <td style="text-align: right;font-weight: bold;" nowrap="nowrap">Rp {{number_format(str_replace("-","",$total_expanditure),2, ',' , '.')}}</td>
            <td>

            </td>
          </tr>

          @foreach($category_expanditure as $expanditure)
          <tr>
            <td style="width: 30px;"></td>
            <td style="font-size: 14px;">
              <span style="font-size: 10px;color:grey;">{{date('j F Y',strtotime($expanditure->date))}}</span> <br>{{substr($expanditure->title,0,50)}} </td>
            <td style="text-align: right;font-size: 14px;" nowrap="nowrap">
              Rp {{number_format(str_replace("-","",$expanditure->journal_transaction),2, ',' , '.')}}
            </td>
            <td nowrap="nowrap" style="font-size: 14px;">
              <a href="{{url('journal/'.$expanditure->journal_id)}}" style="text-decoration: none;">
               <i class="bi-info-circle-fill" style="font-size: 1.2rem;"></i> 
              </a>

              &nbsp;&nbsp;&nbsp;
              <a href="{{url('journal/delete/'.$expanditure->journal_id)}}" style="text-decoration: none;color: red;" onclick="return confirm('Are you sure want to delete {{$expanditure->title}}?')">
               <i class="bi-trash-fill" style="font-size: 1.2rem;"></i> 
              </a>
            </td>
          </tr>
          @endforeach

          <tr>
            <th colspan="2">Debt</th>
            <td style="text-align: right;">Rp 0</td>
            <td>
            </td>
          </tr>

          <tr>
            <th colspan="2">Saving</th>
            <td style="text-align: right;">Rp {{number_format(str_replace("-","",$total_saving),2, ',' , '.')}}</td>
            <td>
            </td>
          </tr>

          @foreach($category_saving as $saving)
          @if($saving->journal_transaction_a > 0)
          <tr>
            <td style="width: 30px;"></td>
            <td style="font-size: 14px;">Discount {{substr($saving->title,0,30)}}</td>
            <td style="text-align: right;font-size: 14px;" nowrap="nowrap">
              Rp {{number_format(str_replace("-","",$saving->journal_transaction_a),2, ',' , '.')}}
            </td>
            <td nowrap="nowrap" style="font-size: 14px;">
              <a href="{{url('journal/'.$saving->journal_id)}}" style="text-decoration: none;">
               <i class="bi-info-circle-fill" style="font-size: 1.2rem;"></i> 
              </a>
            </td>
          </tr>
          @else
          <tr>
            <td style="width: 30px;"></td>
            <td style="font-size: 14px;">{{substr($saving->title,0,30)}}</td>
            <td style="text-align: right;font-size: 14px;" nowrap="nowrap">
              Rp {{number_format(str_replace("-","",$saving->journal_transaction_b),2, ',' , '.')}}
            </td>
            <td nowrap="nowrap" style="font-size: 14px;">
              <a href="{{url('journal/'.$saving->journal_id)}}" style="text-decoration: none;">
               <i class="bi-info-circle-fill" style="font-size: 1.2rem;"></i> 
              </a>
            </td>
          </tr>
          @endif
          @endforeach

          <tr style="background: #df744a;color: white;">
            <th colspan="2">Total</th>
            <td style="text-align: right;">Rp {{number_format($total_amount,2, ',' , '.')}}</td>
            <td></td>
          </tr>
        </table>

       
      </div>
    </div>
</div>

@section('modalBottom1')
      <!-- item option -->
      <div class="row report-date-option" style="background: #e3e3e3a6;text-align: left;display: none;">
        <div class="col-12">
          <table class="table">
            <tr> 
              <td>
                <h3>Report Date</h3>

              </td>
              <td style="text-align: right;">
                <button class="btn btn-danger btn-sm" onclick="setDate()">Close</button>
              </td>
            </tr>
          </table>
         

          <div id="items-list" style="overflow:auto;max-height: 400px;padding: 10px;">
            <form method="post" action="{{url('date-active/update')}}">
            {{CSRF_FIELD()}}
              <div class="form-group mt-3 repeat">
                <label for="select">Duration</label> 
                <div>
                    <select id="duration" name="duration"  class="form-control" onchange="setDuration()">
                      <option selected hidden disabled>Select a duration</option>
                      <option value="one_month">One Month</option>
                      <option value="one_year">One Year</option>
                      <option value="last_month">Last Month</option>
                      <option value="last_year">Last Year</option>
                  </select>

                </div>
              </div>



                <div class="form-group mt-3 option_custom_days" style="display: none;">
                    <label for="name">Custom days</label> 
                    <div>
                        <div class="form-check form-switch">
                      <input class="form-check-input" type="checkbox" id="custom_days" name="custom_days" value="1" onclick="enabledCustomDate()">
                      <label class="form-check-label" for="flexSwitchCheckDefault">Yes</label>
                    </div>
                    </div>
                </div>
                
              <div class="form-group mt-3 custom_days" style="display: none;">
                  <label for="name">Start Days</label> 
                  <input id="start_days" name="start_days" type="number" value="1" required="required" class="form-control" value="" placeholder="input the days">
              </div>

              <div class="form-group mt-3 custom_days" style="display: none;">
                  <label for="name">End Days</label> 
                  <input id="end_days" name="end_days" type="number" value="30" required="required" class="form-control" value="" placeholder="input the days">
              </div>

              <div class="form-group mt-3 last_month" style="display: none;">
                  <label for="name">Total Month</label> 
                  <input id="total_month" name="total_month" type="number" value="2" required="required" class="form-control" value="" placeholder="input total last months">
              </div>

              <div class="form-group mt-3 last_year" style="display: none;">
                  <label for="name">Total Years</label> 
                  <input id="total_years" name="total_years" type="number" value="2" required="required" class="form-control" value="" placeholder="input total last years">
              </div>

              <div class="form-group mt-4">
                <button name="submit" type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
            <br><br><br>
          </div>
          
        </div>
      </div>
@endsection

@section('script2')
<script type="text/javascript">
  function enabledCustomDate(){
    var is_active= $('#custom_days').is(":checked");

    if(is_active){
      $(".custom_days").show();
    }else{
      $(".custom_days").hide();
    }
  }

  function setDate()
  {
    $(".report-date-option").toggle();
  }

  function setDuration()
  {
    var duration = $("#duration").val();

    if(duration == 'one_month'){
      $(".option_custom_days").show();
      $(".last_year").hide();
      $(".last_month").hide();
    }else if(duration == 'one_year'){
      $(".option_custom_days").hide();
      $(".last_year").hide();
      $(".last_month").hide();
    }else if(duration == 'last_month'){
      $(".last_month").show();
      $(".last_year").hide();
      $(".option_custom_days").hide();
    }else if(duration == 'last_year'){
      $(".last_year").show();
      $(".option_custom_days").hide();
      $(".last_month").hide();
    }
    
  }

</script>
@endsection